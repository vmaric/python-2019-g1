from kivy.app import App
from kivy.uix.image import Image
from kivy.core.window import Window
from kivy.clock import Clock 
from kivy.uix.widget import Widget
from kivy.graphics import Color,Rectangle

class Igra(App): 
    def move_left(self):
        velicina = self.img.size[0]
        self.img.pos[0]+=5
        self.img1.pos[0]+=5
        if self.img.pos[0]>velicina-5:
            self.img.pos[0] = -velicina
        if self.img1.pos[0]>velicina-5:
            self.img1.pos[0] = -velicina 
    def move_right(self):
        velicina = self.img.size[0]
        self.img.pos[0]-=5
        self.img1.pos[0]-=5
        if self.img.pos[0]+velicina<0:
            self.img.pos[0] = self.img1.pos[0]+velicina
        if self.img1.pos[0]+velicina<0:
            self.img1.pos[0] = self.img.pos[0]+velicina   

    def mainloop(self,d): 
        if(44 in self.tasteri):
            #if not self.skok and not self.pada:
            self.skok = True
            self.vely = 20
        if(80 in self.tasteri):
            self.sprajt.pos[0]-=4
            self.move_right()
        if(79 in self.tasteri):
            self.sprajt.pos[0]+=4
            self.move_left()  
        spr_img = f"atlas://mojatlas/runningGrant{self.sprajt_frame_index}"

        if self.skok:
            self.sprajt.pos[1] += self.vely
            self.vely-=3
            if self.vely<=0:
                self.pada = True

        if self.pada:
            self.sprajt.pos[1] += self.vely
            if self.sprajt.pos[0] < self.platforma[0]+self.platforma[2]-(self.sprajt.size[0]/2) and self.sprajt.pos[0] > self.platforma[0] and self.sprajt.pos[1] <= (self.platforma[1]+self.platforma[3]) and self.sprajt.pos[1] > (self.platforma[1]):
                self.pada = False
                self.skok = False
                self.sprajt.pos[1] = self.platforma[1]+self.platforma[3]
                self.bindovan = True
        
        if self.bindovan:
            if self.sprajt.pos[0] < self.platforma[0]-(self.sprajt.size[0]/2) or self.sprajt.pos[0] > self.platforma[0]+self.platforma[2]-(self.sprajt.size[0]/2):
                print("detektovano")
                self.pada = True
                self.bindovan = False

        if self.pada:
            if self.sprajt.pos[1]<=self.ground:
                self.sprajt.pada = False
                self.skok = False
                self.sprajt.pos[1]=self.ground


        self.sprajt.source = spr_img 
        self.sprajt_frame_index+=1
        if self.sprajt_frame_index>=13:
            self.sprajt_frame_index = 1

        

    def taster_kliknut(self,a,b,c,d,e): 
        self.tasteri[c] = True
        

    def taster_otpusten(self,a,b,c):
        if c in self.tasteri:
            del self.tasteri[c]

    def build(self): 
        self.tasteri = {}
        root = Widget()

        self.platforma = (200,200,200,20) 
        self.ground = 5
        self.skok = False

        self.img = Image()
        self.img.source = "slikaproba.jpg"
        self.img.allow_stretch = True
        self.img.keep_ratio = False 
        self.img.size_hint = (None,None)
        self.img.size=(1200,600) 
        self.img1 = Image(pos=(-1200,0))
        self.img1.source = "slikaproba.jpg"
        self.img1.allow_stretch = True
        self.img1.keep_ratio = False 
        self.img1.size_hint = (None,None)
        self.img1.size=(1200,600) 

        self.vely = -15
        self.pada = True
        self.bindovan = False

        self.sprajt = Image(pos=(300,500),size_hint=(None,None))
        self.sprajt.allow_stretch = True
        self.sprajt.keep_ratio = False
        self.sprajt.size = (83,165)
        self.sprajt.source = "atlas://mojatlas/runningGrant1" 
        self.sprajt_frame_index = 1
        root.add_widget(self.img)
        root.add_widget(self.img1)

        root.add_widget(self.sprajt)

        root.canvas.add(Color(1,0,0,1))
        root.canvas.add(Rectangle(pos=(200,200),size=(200,20)))

        Window.bind(on_key_down=self.taster_kliknut)
        Window.bind(on_key_up=self.taster_otpusten)

        Clock.schedule_interval(self.mainloop,1/60) 
        return root

Igra().run()