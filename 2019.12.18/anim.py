import time
target=50
x   = 1
spd = 1
while True:
    if x>=target or x<=0:
        spd*=-1
    x+=spd
    print("\r",end="")
    for px in range(target):
        print("#" if px == x else " ", end="")
    time.sleep(1/60)