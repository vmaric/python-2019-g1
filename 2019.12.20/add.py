s = 0 
while True:
    a = input("Unesi broj: ")
    if not a:
        break
    if not a.isnumeric():
        print("Moze samo broj")
        continue  
    a = int(a)
    s += a 
print("Konacan rezultat " + str(s))