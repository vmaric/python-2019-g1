lista = [2,6,3,1,8,4,5,7,15]
parni = []
neparni = []

for i in lista:
    parni.append(i) if (i%2) == 0 else neparni.append(i)

print("Parni:")
print(parni)
print("Neparni:")
print(neparni)