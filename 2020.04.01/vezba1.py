import asyncio

DOCUMENT_ROOT = "D:/python-2019-g1/2020.04.01/www/"
DEFAULT_FILE = "index.html"

TYPES = {
    'jpg':'image/jpeg',
    'txt':'text/plain',
    'html':'text/html'
}

async def req(r,w):
    opis_raw = await r.readline()
    print(opis_raw)
    if not opis_raw:
        print("Connection closed")
        return
    opis_txt = opis_raw.decode("utf-8") 
    opis_parts = opis_txt.split(" ")
    method = opis_parts[0]
    fajl = opis_parts[1].lstrip("/")
    if not fajl:
        fajl = DEFAULT_FILE
    fajl = f"{DOCUMENT_ROOT}{fajl}"
    version = opis_parts[2]
    print(fajl)
    line = await r.readline() 
    line = line.strip().decode("utf-8")
    while line:
        print(line)
        line = await r.readline() 
        line = line.strip().decode("utf-8")
    print("Kraj citanja zahteva") 
    try:
        ext = fajl.split(".")
        ext = ext.pop() 
        mime_type = TYPES[ext]
        doc = open(fajl,"rb")
        content = doc.read()
        print(content)
        content_length = len(content)
        w.write(f"HTTP/1.1 200 Ok\r\nContent-type: {mime_type}\r\nContent-length:{content_length}\r\n\r\n".encode("utf-8"))
        w.write(content)
        doc.close()
    except: 
        w.write(b"HTTP/1.1 404 File Not Found\r\n\r\n") 
    
    w.close()


loop = asyncio.get_event_loop()
print("Starting server...")
server = asyncio.start_server(req,"localhost",80)

loop.run_until_complete(server)

loop.run_forever()
