import unittest
from unittest import TestCase
from bookingapp.booking import Booking,Aranzman,Soba
from unittest import TestSuite,TextTestRunner 

class TestBooking(TestCase):
    b           = Booking() 
    @classmethod
    def setUpClass(cls):
        print("Hello man") 

    @classmethod
    def tearDownClass(cls):
        print("Zavrseno")

    def setUp(self):
        print("Priprema za izvrsavanje metode")

    def tearDown(self):
        print("Kraj izvrsavanja metode")

    def test_izracunaj(self): 
        osoba       = 2
        noc         = 30
        ocekujem    = 60
        dobijam     = self.b.izracunaj_cenu(osoba,noc) 
        self.assertEqual(ocekujem,dobijam) 
    def test_bukiraj(self): 
        vraceno = self.b.bukiraj(123)
        tacno = isinstance(vraceno,Aranzman)
        self.assertTrue(tacno)
