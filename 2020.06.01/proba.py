import redis


# connect with redis server as Bob
bob_r = redis.Redis()
bob_p = bob_r.pubsub()
# subscribe to classical music
bob_p.subscribe('mojkanal')

while True:
    poruka = bob_p.get_message(ignore_subscribe_messages=True)
    if poruka:
        print(poruka)
    