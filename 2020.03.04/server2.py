import socket
heroji = {
    "Fiddlesticks":True,
    "Zed":True,
    "Yasuo":True,
    "Tarik":True
} 

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.bind(("localhost",1234))
server.listen()
print("Server is listening...")
print("avaliable heroes:")
print([k for k in heroji if heroji[k]])
while True:
    client,address = server.accept() 
    message = ",".join([k for k in heroji if heroji[k]])
    client.send(message.encode("utf-8"))
    odabrani_heroj = client.recv(1024).decode("utf-8")
    heroji[odabrani_heroj] = False
    print([k for k in heroji if heroji[k]])
    client.close()