import socket

client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect(("gubitnik.com",1234))

message = client.recv(1024).decode("utf-8")
heroji = message.split(",")
print("Available heroes:")
for i,heroj in enumerate(heroji):
    print(i,heroj)
chosen_hero = heroji[int(input("Pick hero: "))]
client.send(chosen_hero.encode('utf-8'))
