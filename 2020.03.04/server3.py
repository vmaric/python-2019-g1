import socket

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.bind(("localhost",1234))
server.listen()

print("Calculator server v1.0.0")
print("Waiting for connections...")
client,address = server.accept()

brojevi = client.recv(128)
brojevi = brojevi.decode("utf-8")
brojevi = brojevi.split(",")
broj1 = int(brojevi[0])
broj2 = int(brojevi[1])
rezultat = broj1 + broj2

client.send((f"{rezultat}").encode("utf-8"))

server.close()

