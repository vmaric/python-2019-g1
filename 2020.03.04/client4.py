import socket
import time

client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect(("localhost",1234))

while True:
    client.send((f"{time.time()}").encode('utf-8'))
    time.sleep(1)
