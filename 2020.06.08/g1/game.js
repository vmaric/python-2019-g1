var sprajt,bx,div,vayne,btn1,btn2,btn3,skinovi;

function pomeri(){
    bx = parseInt(sprajt.style.backgroundPositionX)
    sprajt.style.backgroundPositionX = (bx-128)+"px"
} 

function ucitajsliku(nazivfajla){
    var xhr = new XMLHttpRequest()
    xhr.open("GET",nazivfajla+".txt?"+Date.now(),false)
    xhr.send(null)  
    sprajt.style.backgroundImage="url('"+xhr.responseText+"')"
}

function ucitajskinove(){
    var xhr = new XMLHttpRequest()
    xhr.open("GET","skinovi.txt?"+Date.now(),false)
    xhr.send(null) 
    var sk = JSON.parse(xhr.responseText)
    skinovi = document.getElementById("skinovi")
    skinovi.onchange = function(){
        ucitajsliku(skinovi.value)
    }
    skinovi.length=0
    s = new Option("Odaberi skin",-1)
    skinovi.add(s)
    for(var i=0;i<sk.length;i++){
        s = new Option(sk[i],i)
        skinovi.add(s)
    }
}

window.addEventListener("load",function(){  

    div = document.createElement("div")
    div.id = "mojdiv" 
    document.body.appendChild(div)  
    
    vayne = document.createElement("img") 
    vayne.id = "vayne"
    vayne.width = 150
    vayne.src= "https://purepng.com/public/uploads/large/purepng.com-vindicator-vayne-skin-lol-splashartsplashartchampionleague-of-legendsskinvayne-331519930886zt15i.png"
    // document.body.appendChild(vayne)
    
    
    
    sprajt = document.createElement("div")
    sprajt.style.backgroundImage="url('https://www.advena.me/advena/wp-content/uploads/2017/10/WalkingManSpriteSheet-2400px-1024x157.png')"
    sprajt.style.backgroundPosition = "20px 0px"
    sprajt.id = "sprajt"
    sprajt.style.width = "128px"
    sprajt.style.height = "157px" 
    sprajt.style.position = "absolute"
    sprajt.style.left = "30px"
    sprajt.style.top = "30px" 
    document.body.appendChild(sprajt)
    
    btn1 = document.createElement("button")
    brojanimacije = 0
    btn1.onclick = function() {
        clearInterval(brojanimacije)
        brojanimacije = setInterval("pomeri()",100)
    }
    btn1.innerHTML = "Start"
    btn1.style.cssText = "padding:4px;margin:4px;"
    div.appendChild(btn1)
    btn2 = document.createElement("button")
    btn2.onclick = function(){
        clearInterval(brojanimacije)
    }
    btn2.style.cssText = "padding:4px;margin:4px;"
    btn2.innerHTML = "Stop"
    div.appendChild(btn2)
    btn3 = document.createElement("button")
    speed = 100
    btn3.onclick = function(){
        clearInterval(brojanimacije)
        brojanimacije = setInterval("pomeri()",speed--)
        if(speed<0){
            speed=100
        }
    }
    btn3.style.cssText = "padding:4px;margin:4px;"
    btn3.innerHTML = "Speed"
    div.appendChild(btn3)
    btn4 = document.createElement("button") 
    btn4.onclick = function(){
        ucitajskinove()
    }
    btn4.style.cssText = "padding:4px;margin:4px;"
    btn4.innerHTML = "Skinovi"
    div.appendChild(btn4)
});