from django.apps import AppConfig


class VezbaappConfig(AppConfig):
    name = 'vezbaapp'
