import http.server
import urllib.parse as parse
import os
import model 

os.chdir("2020.04.27/www")  

class Handler(http.server.SimpleHTTPRequestHandler):
    post_params = {}
    def process_post(self):
        broj_bajtova = int(self.headers["content-length"]) 
        pstring = self.rfile.read(broj_bajtova).decode()
        print(pstring)
        self.post_params = dict(parse.parse_qsl(pstring)) 
    def do_GET(self):
        if self.path.startswith("/activate"):
            parametri = dict(parse.parse_qsl(parse.urlparse(self.path).query))
            token = parametri["token"]
            model.activate(token) 
            self.send_response(200)
            self.end_headers()
            self.wfile.write(b"Bravo care!!!")
        else:    
            super().do_GET()
    def do_POST(self): 
        self.process_post() 
        print(self.post_params)
        username = self.post_params["username"] 
        email = self.post_params["email"] 
        program = self.post_params["program"]
        password = self.post_params["password"]
        omeni = self.post_params["message"]
        over13 = "over13" in self.post_params
        pol = "Zenski" if self.post_params["gender"] == "f" else "Muski"
        output = "Podaci o korisniku:<br>"
        output += f"Ime: {username}<br>"
        output += f"Stariji od 13 godina: {over13}<br>"
        output += f"Pol: {pol}<br>"
        output += f"Program: {program}<br>"

        token = model.register(username,email,password,omeni,1 if self.post_params["gender"] == "f" else 0,program)

        output += f"<a href='http://localhost/activate?token={token}'>Aktivacija</a><br>" 

        self.send_response(200)
        self.send_header("content-type","text/html")
        self.end_headers()
        self.wfile.write(output.encode())

http.server.HTTPServer(("localhost",80),Handler).serve_forever()