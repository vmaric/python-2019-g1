import mysql.connector as conn
import uuid

def cnn():
    return conn.connect(user="root",passwd="",host="localhost",database="korisnici")

def register(username,email,passwd,omeni,pol,program):
    db = cnn()
    cur = db.cursor()
    token = uuid.uuid1().hex
    cur.execute("insert into korisnici values (null,%s,%s,%s,%s,%s,%s,0,%s)",(username,email,passwd,omeni,pol,program,token))
    db.commit()
    db.close()
    return token

def activate(token):
    db = cnn()
    cur = db.cursor() 
    cur.execute("update korisnici set active = 1 where token = %s and active = 0",(token,))
    db.commit()
    db.close()
    return token