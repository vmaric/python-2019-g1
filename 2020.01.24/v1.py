class Placanje:
    balance = 0
    def validate(self,am):
        return am <= self.balance
    def plati(self,am):
        print("Placas ",am)
class Kartica(Placanje):
    def gettax(self):
        return 1
    def plati(self,am):
        if self.validate(am):
            self.balance-=(am*self.gettax())
            print("successfull payment")
        else:
            print("Not enough funds")
class Wallet(Placanje):
    def plati(self,am):
        if self.validate(am):
            self.balance -= am
            print("Success")
        else:
            print("Failure")

class Visa(Kartica):
    def gettax(self):
        return 1.1

p = Wallet()
p.balance = 100
p.plati(10)
print("New balance: ",p.balance)