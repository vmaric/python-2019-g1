class Player:
    def __init__(self,name):
        self.pos = (0,0)
        self.name = name
        self.health = 0
        self.damage = 0

    def __str__(self):
        return self.name + " " + str(self.health)

    def doextras(self):
        pass
    
    def hit(self,target):
        self.doextras()
        target.health -= self.damage

class SubZero(Player):

    def __eq__(self,other):
        return self.health == other.health

    def doextras(self):
        self.rage +=  10

    def __add__(self,other):
        novi = SubZero()
        novi.name = "PoweredSubZero"
        novi.health = self.health + other.health
        return novi
    
    def __str__(self):
        return super().__str__() + " " + str(self.rage)

    def __init__(self):
        super().__init__("SubZero")
        self.rage = 0
        self.damage = 20
        self.health = 100

class Scorpion(Player):
    def __str__(self):
        return super().__str__() + " " + str(self.mana)
    def doextras(self):
        self.mana -= 8
    def __init__(self):
        super().__init__("Scorpion")
        self.mana = 80
        self.damage = 30
        self.health = 115