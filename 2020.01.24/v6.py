class Point:
    def __init__(self):
        self.__x = 0
        self.__y = 0

    @property
    def y(self):
        return self.__y

    @y.setter
    def y(self,val):
        self.__y = val

    def getx(self):
        return self.__x

    def setx(self,val):
        if isinstance(val,int):
            self.__x = val

    def draw(self):
        print(self.__x,self.__y) 

pt = Point()
pt.y = 10
pt.draw()