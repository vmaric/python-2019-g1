class Melee:
    def hit(self):
        print("Udarac iz blizine")

class Range:
    def hit(self):
        print("Udarac iz daljine")

class Jayce(Range,Melee):
    def hit(self):
        super().hit()

def f(o):
    print(o)

Melee.f = f
m = Melee()
m.f()
