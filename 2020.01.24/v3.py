import abc
class Product(abc.ABC):
    def __init__(self,name,price):
        self.name   = name
        self.price  = price 
    @abc.abstractmethod
    def porez(self):
        pass
    def kupi(self):
        print("Kupujes cipele",self.name,"po ceni od",self.porez(),"dindzi")

class Cipele(Product):
    def porez(self):
        return self.price * 1.1
    
p = Cipele("Cipele",100)
p.kupi()
