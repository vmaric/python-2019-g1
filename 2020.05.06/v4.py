from sklearn import naive_bayes
import sklearn.metrics

x = [
    [1,1,1],
    [0,1,1],
    [1,0,0],
    [0,0,1]
]
y = [1,1,0,0]

model = naive_bayes.GaussianNB()
model.fit(x,y)

pobeda = model.predict([[1,0,1],[1,1,1],[0,0,1]])

print(sklearn.metrics.confusion_matrix([0,1,1],pobeda))