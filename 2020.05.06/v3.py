import sklearn.linear_model
import sklearn.metrics
import sklearn.model_selection


x = [
    [1],[2],[3],[4]
]
y = [2,4,6,8]
 

u_x, t_x, u_y, t_y = sklearn.model_selection.train_test_split(x,y,train_size=0.5)

model = sklearn.linear_model.LinearRegression()
model.fit(u_x,u_y)

y_ = model.predict(t_x) 
 
print(y_)
print(t_y)