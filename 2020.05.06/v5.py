import sklearn.datasets 
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
iris = sklearn.datasets.load_iris() 

x = iris["data"]
y = iris["target"]

model = SVC()
model.fit(x,y)

cvet = model.predict([[5.2,2.7,3.9,1.3]])

print(iris["target_names"][cvet.item()])

