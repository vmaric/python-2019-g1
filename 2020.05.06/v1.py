import numpy as np

x = np.array([20,30,40,50,60])
y = np.array([5,5,3,-4,-5])

n = len(x)

a = (y.sum()*(x**2).sum())
a -= (x.sum()*(x*y).sum())
a /= n*(x**2).sum() - x.sum()**2

b = n*(x*y).sum()
b -= (x.sum()*y.sum())
b /= n*(x**2).sum() - x.sum()**2

x_ = 45 

y_ = a + (b * x_)

y_ = 1 / (1 + np.exp(-y_))

print(y_>0.5)