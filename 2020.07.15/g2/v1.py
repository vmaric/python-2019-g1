from flask import Flask,request 
import mysql.connector as conn
import json

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="ministarstvo")  

app = Flask(__name__)

korisnici = {
    "dovlacar":"Dovla",
    "markotigar":"Marko"
}

@app.route("/ministarstvo/<int:mid>",methods=["GET","POST","PUT","DELETE"])
def ministarstvobyid(mid):
    metod = request.method.lower()
    if metod == "get":
        baza = getconnection()
        cur = baza.cursor()
        cur.execute("select * from ministarstva where id = %s",(mid,))  
        res = cur.fetchone()
        baza.close() 
        output = {"error":"User does not exist"}
        if res:
            mid,naziv,ministar = res
            output = {"id":mid,"naziv":naziv,"ministar":ministar} 
        return json.dumps(output),200,{"Content-type":"application/json"}  
    

@app.route("/ministarstvo",methods=["GET","POST","PUT","DELETE"])
def ministarstvo():
    if not "kljuc" in request.args:
        return '{"error":"di ti je kljuc?"}',401,{"Content-type":"application/json"}
    if not request.args["kljuc"] in korisnici:
        return '{"error":"ne ljava ti kljuc?"}',401,{"Content-type":"application/json"}
    print("Zahtev od korisnika",korisnici[request.args["kljuc"]])
    metod = request.method.lower()
    if metod == "get":
        baza = getconnection()
        cur = baza.cursor()
        cur.execute("select * from ministarstva") 
        output = [] 
        for mid,naziv,ministar in cur.fetchall():
            output.append({"id":mid,"naziv":naziv,"ministar":ministar}) 
        baza.close() 
        return json.dumps(output),200,{"Content-type":"application/json"}
    if metod == "post":
        novo_m = request.get_json() 
        naziv = novo_m["naziv"]
        ministar = novo_m["ministar"]
        baza = getconnection()
        cur = baza.cursor()
        cur.execute("insert into ministarstva values (null,%s,%s)",(naziv,ministar))   
        baza.commit()
        baza.close()
        return '{"success":1}',200,{"Content-type":"application/json"}
    if metod == "put":
        novo_m = request.get_json() 
        mid = novo_m["id"]
        naziv = novo_m["naziv"]
        ministar = novo_m["ministar"]
        baza = getconnection()
        cur = baza.cursor()
        cur.execute("update ministarstva set naziv=%s,ministar=%s where id = %s",(naziv,ministar,mid))   
        baza.commit()
        baza.close()
        return '{"success":1}',200,{"Content-type":"application/json"}
    if metod == "delete":  
        mid = request.args["id"]
        baza = getconnection()
        cur = baza.cursor()
        cur.execute("delete from ministarstva where id = %s",(mid,))   
        baza.commit()
        baza.close()
        return '{"success":1}',200,{"Content-type":"application/json"}

app.run(debug=True)