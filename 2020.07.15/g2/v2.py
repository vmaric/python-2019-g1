from flask import Flask,request,render_template
import os
import requests
import json

os.chdir("D:/python-2019-g1/2020.07.15/g2")

app = Flask(__name__)

kljuc = "dovlacar"

@app.route("/sajt")
def sajt():

    return render_template("sajt.html")


@app.route("/api")
def api():
    if "id" in request.args:
        mid = request.args["id"]
        data = requests.get(f"http://127.0.0.1:5000/ministarstvo/{mid}?kljuc={kljuc}") 
        return data.text ,200,{"Content-Type":"application/json"}
    else:
        data = requests.get(f"http://127.0.0.1:5000/ministarstvo?kljuc={kljuc}") 
        data = data.json()
        for d in data:
            del d["ministar"]
        return json.dumps(data),200,{"Content-Type":"application/json"}

@app.route("/")
def page():  
    if "id" in request.args:
        mid = request.args["id"]
        data = requests.get(f"http://127.0.0.1:5000/ministarstvo/{mid}?kljuc={kljuc}")
        return render_template("ministarstvo.html",ministarstvo=data.json())
    else:
        data = requests.get(f"http://127.0.0.1:5000/ministarstvo?kljuc={kljuc}")
        allministarstva = data.json()
        return render_template("index.html",ministarstva=allministarstva)



app.run(debug=True,port=80)