from flask import Flask,request
import mysql.connector as conn
import json

app = Flask(__name__)

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="shop2")  

@app.route("/proizvod",methods=['GET','POST','PUT','DELETE'])
def home():
    mtd = request.method.lower()
    if mtd=="get":
        db = getconnection()
        cur = db.cursor()
        pid = ""
        if "id" in request.args:
            pid = f" where id in({request.args['id']})" 
        cur.execute(f"select * from products{pid}") 
        output = []
        for id,naziv,cena,kolicina in cur.fetchall():
            output.append({
                "id":id,"naziv":naziv,"kolicina":kolicina,"cena":float(cena)
            }) 
        rezultat = json.dumps(output)
        db.close() 
        return rezultat, 200, {"Content-Type":"application/json" }
    if mtd=="post":
        data = request.get_json()
        db = getconnection()
        cur = db.cursor()
        cur.execute("insert into products values (null,%s,%s,%s)",(data["naziv"],data["cena"],data["kolicina"]))
        db.commit()
        db.close()
        return '{"success":1}'
    if mtd=="put":
        return "UPDATE"
    if mtd=="delete":
        return "DELETE"

app.run(debug=True)