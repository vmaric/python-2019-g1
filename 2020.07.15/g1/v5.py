import mysql.connector as conn 
from flask import Flask,request 

app = Flask(__name__)

@app.route("/")
def home():
    strana = "<h2>Welcome to game page</h2>"
    strana += "Active users: <span id='au'>123</span>"
    strana += "<script>var ws = new WebSocket('ws://localhost:8765'); ws.onmessage = function(data){ document.getElementById('au').innerHTML = parseInt(document.getElementById('au').innerHTML)+1; } </script>"
    return strana

app.run(debug=True,port=85)
