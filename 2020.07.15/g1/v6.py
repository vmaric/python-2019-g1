import redis
import time
import asyncio
import websockets 

r = redis.Redis()

ps = r.pubsub()
ps.subscribe("logovanja")

def poruka(poruka):
    sadrzaj = poruka["data"].decode()
    for k in korisnici:
        asyncio.run(k.send(sadrzaj))

ps.subscribe(logovanja=poruka) 
ps.run_in_thread(0.001)

korisnici = {
    
}

async def konekcija(websocket, path):
    korisnici[websocket] = websocket
    try:
        while True:
            print(await websocket.recv())
    except:
        del korisnici[websocket]

start_server = websockets.serve(konekcija, "localhost", 8765) 
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()