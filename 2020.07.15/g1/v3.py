from flask import Flask,request
import mysql.connector as conn
import json
import secrets
import redis
import jwt
import time
import stomp

app = Flask(__name__)

rds = redis.Redis()
 

stompconn = stomp.Connection()
stompconn.connect('admin','admin',wait=True)

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="shop2")  
        

@app.route("/",methods=['POST'])
def login():  
    data = request.get_json()
    un = data["username"]
    pwd = data["password"]
    db = getconnection()
    cur = db.cursor() 
    cur.execute("select * from users where username=%s and passwd=%s",(un,pwd))
    user = cur.fetchone()
    cur.close()
    db.close() 
    karta = secrets.token_hex(16)
    rds.set(karta,json.dumps({"id":user[0],"username":user[1],"balance":float(user[3])}))
    rds.expire(karta,10) 
    karta = jwt.encode({"karta":karta,"exp":time.time()+10},"nastajnikljuc","HS256") 
    if user:

        rds.publish("logovanja","logovanje")

        stompconn.send(body=f"ulogovao se {un} u {time.time()}",destination="/queue/logovanja")
 
        return '{"success":"Ok","karta":"'+karta.decode()+'"}'
    else:
        return '{"error":"Invalid user"}' 
    

app.run(debug=True)