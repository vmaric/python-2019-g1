from flask import Flask,request
import mysql.connector as conn
import json

app = Flask(__name__)

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="shop2")

merchants = {
    "12345":"Pavle",
    "23456":"Dovla"
}

def checkKey():
    if "key" in request.args and request.args["key"] in merchants:
        return merchants[request.args["key"]]
    else:
        return False
        

@app.route("/login",methods=['GET','POST','PUT','DELETE'])
def home():
    merchant = checkKey()
    if not merchant:
        return '{"error":"Invalid Key"}'
    korisnici = ["Pera","Mika","Laza"]
    return json.dumps(korisnici)

app.run(debug=True)