import urllib.request as req
from flask import Flask,request,url_for,redirect
import json



app = Flask(__name__)

@app.route("/",methods=["POST","GET"])
def mainpage():
    if request.method.lower()=="post":
        un = request.form["username"]
        pwd = request.form["password"] 
        r = req.Request("http://127.0.0.1:5000")
        r.add_header("Content-Type","application/json")
        r.method = "POST"
        r.data = json.dumps({"username":un,"password":pwd}).encode()
        res = req.urlopen(r)
        resdata = json.loads(res.read())
        return "",303, {"location":"http://localhost:82/"+resdata["karta"]}
    else:
        return "<form method='post'><input type='text' name='username'/><input type='text' name='password'/><input type='submit' value='Login'/></form>"

app.run(debug=True,port=80)