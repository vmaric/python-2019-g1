from flask import Flask,request,session
import mysql.connector as conn
import json
import redis
import jwt

app = Flask(__name__)
app.config["SECRET_KEY"] = "12345"

rds = redis.Redis()

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="shop2")  

@app.route("/<int:a>/<int:b>",methods=['GET','POST','PUT','DELETE'])
def home(a,b):
    if not "user" in session:
        return "Ne moze bato"
    return str(a + b)

@app.route("/<karta>")
def init(karta): 
    user = jwt.decode(karta,"nastajnikljuc","HS256")
    user = rds.get(user["karta"])
    if not user:
        return "Invalid user"
    session["user"] = json.loads(user)
    return f"you are logged in {user}"

app.run(debug=True,port=82)