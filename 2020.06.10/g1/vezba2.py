import tkinter

prozor = tkinter.Tk()

tekst = tkinter.Label(prozor,text="Dovla!",bg="red")
tekst.grid(row=0,column=0)
tekst1 = tkinter.Label(prozor,text="Milan!",bg="yellow")
tekst1.grid(row=0,column=1)
tekst2 = tkinter.Label(prozor,text="Sasa!",bg="green")
tekst2.grid(row=1,column=0,columnspan=2,sticky="we")

f = tkinter.Frame(prozor)
f.grid(row=2,column=0,columnspan=2,sticky='we')

for i in range(3):
    l = tkinter.Label(f,text=f"{i}",bg="blue")
    l.pack(expand=True,side="left",fill="x")
 
prozor.mainloop()