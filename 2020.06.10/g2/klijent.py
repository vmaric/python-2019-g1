import socket
from threading import Thread
import os
import time

ADDR = "gubitnik.com"
PORT = 9999

client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

username = input("Username: ")
client.sendto(f"1|{username}".encode("utf-8"), (ADDR, PORT))

message_queue = []
heart_msg = "Sending heartbeat"
msg = ""


def receiver():
    while True:
        try:
            nova_poruka = client.recv(128).decode("utf-8")
            if len(message_queue) > 10:
                message_queue.pop(0)
            message_queue.append(nova_poruka)
            os.system("cls")
            for msg in message_queue:
                msg_parts = msg.split("|")
                print(f"{msg_parts[1]}: {msg_parts[2]}")
            print("Poruka: ")
        except:
            break


def heartbeat():
    while True:
        try:
            client.sendto(f"4|{heart_msg}".encode("utf-8"), (ADDR, PORT))
            for i in range(200):
                if close_threads:
                    return

                time.sleep(0.01)
        except:
            break


t1 = Thread(None, receiver)
t2 = Thread(None, heartbeat)
t1.start()
t2.start()

close_threads = False

print("Poruka: ")

while True:
    msg = input()

    if not msg:
        print("Bye bye")
        client.sendto(f"3".encode("utf-8"), (ADDR, PORT))
        close_threads = True
        break

    client.sendto(f"2|{msg}".encode("utf-8"), (ADDR, PORT))


client.close()

