import socket
import enum
import time

ADDR = "localhost"
PORT = 9999 


class MessageType(enum.Enum):
    REGISTER = 1
    TXT = 2
    LOGOUT = 3

class Message:
    def __init__(self,tp,txt="",un=""):
        self.tp = tp
        self.txt = txt
        self.username = un 
    @staticmethod
    def parse(msg): 
        msg_parts = msg.split("|")
        tp = MessageType(int(msg[0]))
        username = ""
        txt = ""
        if tp == MessageType.REGISTER:
            username = msg_parts[1]
        if tp == MessageType.TXT:
            txt = msg_parts[1]
        res = Message(tp,txt,username) 
        return res 
    def render(self):
        if self.tp == MessageType.REGISTER:
            return f"{self.tp.value}|{self.username}"
        elif self.tp == MessageType.TXT:
            return f"{self.tp.value}|{self.txt}"
        else:
            return f"{self.tp.value}"

class Korisnik:
    def __init__(self,username):
        self.username = username
        self.last_ack = time.time()
 
class ChatServer:
    def __init__(self,addr,port):
        self.korisnici = {}  
        self.server = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.server.bind((addr,port)) 

    def broadcast(self,msg):
        for addr in self.korisnici:
            self.server.sendto(msg,addr)

    def run(self):
        while True:
            msg,addr = self.server.recvfrom(128)
            msg = Message.parse(msg.decode("utf-8"))
            if msg.tp == MessageType.REGISTER:
                novi_korisnik = Korisnik(msg.username)
                print(f"Korisnik {novi_korisnik.username} je usao u sobu")
                self.korisnici[addr] = novi_korisnik
            if msg.tp == MessageType.TXT: 
                korisnik = self.korisnici[addr]
                print("Poruka od korisnika",korisnik.username)
                self.broadcast(f"2|{korisnik.username}|{msg.txt}".encode("utf=8"))
                print(msg.txt) 
            if msg.tp == MessageType.LOGOUT:
                korisnik = self.korisnici[addr]
                del self.korisnici[addr]
                print(f"Korisnik {korisnik.username} je otisao") 



ChatServer(ADDR,PORT).run()