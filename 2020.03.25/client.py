import socket
import time
from threading import Thread
import os



ADDR = "gubitnik.com"
PORT = 9999 

client = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 

client.sendto(f"1|Vlada".encode("utf-8"),(ADDR,PORT))

message_queue = []

def receiver():
    print("receiving messages")
    while True:
        nova_poruka = client.recv(128).decode("utf-8")
        if len(message_queue) > 10:
            message_queue.pop(0)
        message_queue.append(nova_poruka)
        os.system("cls")
        for msg in message_queue:
            print(msg)
        print("Poruka:")


Thread(None,receiver).start() 
print("Poruka")
while True: 
    msg = input()
    if not msg:
        break
    client.sendto(f"2|{msg}".encode("utf-8"),(ADDR,PORT)) 
client.sendto(f"3".encode("utf-8"),(ADDR,PORT))
client.close()