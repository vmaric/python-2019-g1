from django.shortcuts import render
from django.shortcuts import HttpResponse
from . import models
import time

def index(req): 
    print(req.session)
    username = req.session["username"]
    return render(req,"strana.html",{"ime":username,"vreme":time.time()})

def login(req):
    if req.GET["p"] == "123":
        req.session["username"] = req.GET["u"] 
    return HttpResponse(f"Login fajl")

def proba(req):
    p = models.Products()
    p.naziv = "Telefon"
    p.save()
    return HttpResponse(f"Unet telefon")

def proba1(req):
    p = models.Products.objects.get(pk=1)
    print(p.naziv)
    return HttpResponse(f"Unet telefon")
