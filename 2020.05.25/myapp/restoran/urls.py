from django.contrib import admin
from django.urls import path, include
from django.shortcuts import HttpResponse
from . import views

urlpatterns = [ 
    path('',views.index),
    path('login',views.login),
    path('proba',views.proba),
    path('proba1',views.proba1)
]
