import time
import abc

class Subscriber(abc.ABC):
    @abc.abstractmethod
    def update(self):
        pass

class LoLGame:
    def __init__(self):
        self.__slusaci = []
    def add5minutalistener(self,slusac):
        self.__slusaci.append(slusac)
    def remove5minutalistener(self,slusac):
        self.__slusaci.remove(slusac)
    def jos_5_minuta(self):
        for slusac in self.__slusaci:
            slusac.update()
    def start(self):
        print("LoL game has been started")
        gametime = 10
        while gametime > 0:
            time.sleep(1)
            if gametime == 5:
                self.jos_5_minuta()
            print(gametime,"seconds left")
            gametime-=1
        print("Game finished")

class Semafor(Subscriber):
    def update(self):
        print("Jos 5 sekundi momci")
class Mreza(Subscriber):
    def update(self):
        print("Jos 5 minuta, info ide na mrezu")

lol = LoLGame() 

lol.add5minutalistener(Semafor())
lol.add5minutalistener(Mreza())
s1 = Semafor()
lol.add5minutalistener(s1)
lol.remove5minutalistener(s1)

lol.start()
