import mysql.connector as conn

db = conn.connect(
    database="g1_prodavnica",
    host="localhost",
    user="root",
    passwd=""
) 
cur = db.cursor() 

po_strani = 3

while True:
    strana = int(input("Unesi stranu: "))
    strana = (strana-1) * po_strani
    cur.execute(f"select * from products limit {strana},{po_strani}")
    res = cur.fetchall()
    for i in res:
        print(i[0],i[1],i[2],i[3])

db.close()