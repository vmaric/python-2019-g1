match = {
    "home":"Manchester United",
    "away":"Chelsea",
    "score":{
        "home":1,
        "away":2
    }
}

print("Utakmica:")
print(match["home"]+" vs " + match["away"])
print(match["score"]["home"],":",match["score"]["away"])