import hashlib  
user = {
    "ime":"pera",
    "sifra":hashlib.md5(b"123").hexdigest()
} 

un = "pera"
up = "1234"
up = hashlib.md5(up.encode('utf-8')).hexdigest()

valid_user = un == user["ime"] and up == user["sifra"]

if valid_user:
    print("Welcome")
else:
    print("Invalid user")