users = [
    ["peter",100],
    ["sally",50],
    ["john",70],
    ["mark",115],
    ["vladimir",5]
]
ul = len(users) 
while True:
    has_changes = False
    for i in range(1,ul):
        if users[i][1]>users[i-1][1]:
            tmp = users[i]
            users[i] = users[i-1]
            users[i-1] = tmp
            has_changes = True

    if not has_changes:    
        break
for user in users:
    print(user[0],user[1])