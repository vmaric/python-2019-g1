from django.shortcuts import render
from django.shortcuts import HttpResponse
import datetime
from . import models

# Create your views here.
def index(request):
    vreme = datetime.datetime.now() 
    f = render(request,"index.html",{"vreme":vreme,"kategorije":models.getkategorije(),"active1":"active"})
    return f

def bycat(request,cat): 
    vreme = datetime.datetime.now() 
    f = render(request,"index.html",{"proizvodi":models.getproizvodi()[cat],"vreme":vreme,"kategorije":models.getkategorije(),"active1":"active"})
    return f

def about(request):
    sablon = render(request,"about.html",{"active2":"active"})
    return sablon

def contact(request):
    sablon = render(request,"contact.html",{"active3":"active"})
    return sablon