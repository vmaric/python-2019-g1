from django.apps import AppConfig


class MojsajtConfig(AppConfig):
    name = 'mojsajt'
