from django.db import models  

class Proizvod:
    def __init__(self,pid,naziv):
        self.pid = pid
        self.naziv = naziv
    def __str__(self):
        return self.naziv

class Kategorija:
    def __init__(self,kid,naziv):
        self.kid = kid
        self.naziv = naziv
    def __str__(self):
        return self.naziv

def getproizvodi():
    return {
        "1":[Proizvod(1,"Capriciossa"),Proizvod(2,"Diavollo"),Proizvod(3,"Vegetariana"),Proizvod(4,"Vegetariana")],
        "2":[Proizvod(5,"Ceasar"),Proizvod(6,"Spring Salad"),Proizvod(7,"Fruit Salad"),Proizvod(8,"Fruit Salad")],
        "3":[Proizvod(9,"Indomie"),Proizvod(10,"Migoreng"),Proizvod(11,"Neke tamo nudle"),Proizvod(12,"Neke tamo nudle")]
    }

def getkategorije():
    return [Kategorija(1,"Pizza"),Kategorija(2,"Salad"),Kategorija(3,"Noodle")]