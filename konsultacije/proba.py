import mysql.connector as conn
DBHOST="localhost"
DBUSER="root"
DBPASS=""
DATABASE="sakila"

class Database:
    konekcija = None
    @staticmethod
    def getConnection(): 
        if not Database.konekcija: 
            Database.konekcija = conn.connect(host=DBHOST,username=DBUSER,passwd=DBPASS,database=DATABASE) 
        return Database.konekcija

for i in range(1):
    db = Database.getConnection()   
    cur = db.cursor()
    db.close()


