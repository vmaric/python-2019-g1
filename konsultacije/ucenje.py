import math
podaci = (
    (2,9,1),
    (3,8,1),
    (8,2,0),
    (9,1,0),
    (3,6,1),
    (2,6,1)
)

def n(v):
    return 1 / (1 + math.exp(v))

w = [0.2,0.8]

slucaj = podaci[0]
pred = w[0]*slucaj[0] + w[1]*slucaj[1]
