import asyncio
import time
import _thread

async def f():
    print("cao")
    await asyncio.sleep(10)
    print("kako je")

async def f1():
    print("Kako je?") 

async def glavna():
    asyncio.create_task(f())
    asyncio.create_task(f1())

loop = asyncio.get_event_loop()

loop.run_until_complete(glavna())

loop.run_forever()


