import numpy as np

x = np.array([1,2,3,4,5,6])
y = np.array([2,4,6,8,10,12])

n = len(x)

a = (y.sum()*(x**2).sum())
a -= (x.sum()*(x*y).sum())
a /= n*(x**2).sum() - x.sum()**2

b = n*(x*y).sum()
b -= (x.sum()*y.sum())
b /= n*(x**2).sum() - x.sum()**2

x_ = 100

y_ = a + (b * x_)

print(y_)