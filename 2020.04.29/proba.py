import matplotlib.pyplot as plt
import io  
import http.server
import base64
import random


buffer = io.BytesIO() 

#buffer.seek(0)
# dijagram = base64.encodebytes(buffer.read()).decode()
# print(dijagram)
# exit(0)
  
kladjenje = []

class Handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        
        self.send_response(200)
        self.send_header("content-type","text/html")
        self.end_headers()
        plt.clf()
        buffer.seek(0)
        plt.plot([1,2,3,4])  
        plt.savefig(buffer)
        buffer.seek(0)
        dijagram = base64.encodebytes(buffer.read()).decode() 
        self.wfile.write(b"<h3>Statistike kladjenja</h3>")
        slika = f"<img width=300 src='data:image/png;base64,{dijagram}' />" 
        self.wfile.write(slika.encode())
        self.wfile.write(b"<h3>Uspeh kladionice</h3>")
        kladjenje.append(random.randint(0,100))
        plt.clf()
        plt.plot(kladjenje)
        buffer.seek(0)
        plt.savefig(buffer)
        buffer.seek(0)
        dijagram = base64.encodebytes(buffer.read()).decode() 
        slika = f"<img width=300 src='data:image/png;base64,{dijagram}' />" 
        self.wfile.write(slika.encode())

http.server.HTTPServer(("localhost",80),Handler).serve_forever()