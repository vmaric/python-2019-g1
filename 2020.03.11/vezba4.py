import asyncio

async def zahtev(r,w):
    procitano = await r.read(1024)
    print(procitano)
    w.write(b"HTTP/1.1 200 Ok\r\n\r\nCao i tebi")
    w.close()

server = asyncio.start_server(zahtev,host="localhost",port=8005) 
loop = asyncio.get_event_loop()
loop.run_until_complete(server)
loop.run_forever()

