import socket
import select
import time

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
server.setblocking(0)
server.bind(("localhost",8005))
server.listen()

citanje = [server]
pisanje = []
greske = []

while True:
    c,p,g = select.select(citanje,pisanje,greske)
    for sock in c:
        if sock == server:
            noviklijent,_ = sock.accept()
            noviklijent.setblocking(0)
            citanje.append(noviklijent)
        else: 
            print(sock.recv(1024)) 
            sock.send(b'Hello i tebi')
            citanje.remove(sock)


