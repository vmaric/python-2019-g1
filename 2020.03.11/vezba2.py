import socket
import time
import selectors
import random

server = socket.socket()
server.bind(("localhost",8005))
server.setblocking(0)
server.listen()

players = {
    
}

def broadcast(msg):
    for player in players:
        player.send(msg.encode('utf-8'))

def igrac_govori(sock):
    procitano = sock.recv(128)
    if not procitano:
        sock.close()
        mojselektor.unregister(sock)
        del players[sock]
    procitano = procitano.decode('utf-8')
    procitano = procitano.split("|")
    poruka = int(procitano[0])
    sadrzaj = procitano[1]
    if poruka == 1:
        sock.send((f"Vreme: {time.time()}").encode("utf-8"))
    elif poruka == 2:
        players[sock] = sadrzaj.split(",")


def stigao_klijent(sock):
    novi_igrac,addr = sock.accept()
    novi_igrac.setblocking(0)
    players[novi_igrac] = "Hello"
    mojselektor.register(novi_igrac,selectors.EVENT_READ,igrac_govori)

mojselektor = selectors.DefaultSelector()

mojselektor.register(server,selectors.EVENT_READ,stigao_klijent)

brojevi = [x for x in range(1,20)]
random.shuffle(brojevi)

izvuceni = []
print(brojevi)

def game_update():
    if len(brojevi)<1:
        print("Gotova igra")
        exit(0) 

    izvuceni.append(brojevi.pop())
    print(izvuceni)
    
    for covek in players:
        pogodjeni = []
        covek_brojevi = players[covek]
        for broj in covek_brojevi:
            if int(broj) in izvuceni:
                pogodjeni.append(broj)
        if pogodjeni:
            covek.send((",".join(pogodjeni)).encode("utf-8"))    

staro_vreme = int(time.time())

while True:
    dogadjaj = mojselektor.select(0.001)
    for dog in dogadjaj:
        dolazni_soket = dog[0].fileobj
        callback_funkcija = dog[0].data
        callback_funkcija(dolazni_soket)
    novo_vreme = int(time.time())
    if novo_vreme - staro_vreme >= 2:
        staro_vreme = int(time.time())
        game_update()
