import asyncio
import time 

async def hello():
    print("Hello world")
    await asyncio.sleep(3)
    print("How are you")


async def mojprogram():
    task1 = asyncio.create_task(hello())
    task2 = asyncio.create_task(hello())
    await task1
    await task2

asyncio.run(mojprogram())