from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.pagelayout import PageLayout

class MyApp(App):
    def build(self):
        root = PageLayout()

        korisnici = BoxLayout()
        korisnici.add_widget(Button(text="Unesi korisnika"))
        korisnici.add_widget(Button(text="izbaci korisnika"))

        gameplay = BoxLayout()
        gameplay.add_widget(Button(text="Start game"))
        gameplay.add_widget(Button(text="End game"))

        about = BoxLayout()
        about.add_widget(Button(text="About us"))
        about.add_widget(Button(text="About you"))

        root.add_widget(korisnici)
        root.add_widget(gameplay)
        root.add_widget(about)

        return root 

MyApp().run()