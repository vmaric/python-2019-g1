from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout

class MyApp(App):
    def build(self):
        root = RelativeLayout() 
        root.pos = (200,100) 
        btn = Button(text="Click me")
        btn.size_hint = (None,None)  
        btn.pos = (200,200) 
        root.add_widget(btn) 
        return root 

MyApp().run()