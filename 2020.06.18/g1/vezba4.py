from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout

class MyApp(App):
    def build(self):
        root = GridLayout(cols=4,rows=5)

        for i in range(20):
            taster = Button(text=f"taster {i}")
            root.add_widget(taster)

        return root


MyApp().run()