from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.pagelayout import PageLayout
from kivy.graphics import Color,Rectangle
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen,ScreenManager
from kivy.clock import Clock
Window.size = (300,120) 

class MyApp(App):
    sm = ScreenManager()
    def cbf(self,d):
        print("Hello",d)
    def login(self,sender):
        print(self.uname.text)
        self.sm.transition.direction = "left"
        self.sm.current = "userscreen"
    def back_to_login(self,sender):
        self.sm.transition.direction = "right"
        self.sm.current="loginscreen"
    def build(self): 
        Clock.schedule_interval(self.cbf,1)
        user_screen = Screen(name="userscreen")
        user_screen.add_widget(Button(text="Click me",on_press=self.back_to_login))
        login_screen = Screen(name="loginscreen") 
        root = BoxLayout(orientation="vertical") 
        lbl = Label(text="Welcome to our app!!!")
        root.add_widget(lbl) 
        self.uname   = TextInput()
        self.passwd  = TextInput() 
        root.add_widget(self.uname)
        root.add_widget(self.passwd)
        self.uname.bind(text=lbl.setter('text'))
        taster  = Button(text="Dovla")    
        taster.bind(on_press=self.login) 
        root.add_widget(taster) 
        login_screen.add_widget(root)
        self.sm.add_widget(login_screen)
        self.sm.add_widget(user_screen)
        return self.sm

MyApp().run()