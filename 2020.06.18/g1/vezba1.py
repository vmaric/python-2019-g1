from kivy.properties import StringProperty
from kivy.event import EventDispatcher


class Dovla(EventDispatcher):
    prop = StringProperty() 

def callback(sender, args):
    print(sender,args)

d = Dovla()
d.prop = "World"
d.bind(prop=callback)
d.prop = "Hello"
print(d.prop)