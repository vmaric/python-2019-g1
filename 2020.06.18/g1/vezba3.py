from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout

class MyApp(App):
    def build(self):
        box = BoxLayout(orientation="vertical") 
        topmeni = BoxLayout(orientation="horizontal") 
        for i in range(5):
            taster = Button(text="Click me") 
            topmeni.add_widget(taster)
        box.add_widget(topmeni)
        gameplay = Button(text="Start game")
        gameplay.size_hint=(1,9)
        box.add_widget(gameplay)

        return box


MyApp().run()