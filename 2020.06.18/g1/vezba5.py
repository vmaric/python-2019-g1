from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.stacklayout import StackLayout

class MyApp(App):
    def build(self):
        root = StackLayout(orientation="bt-rl")

        for i in range(20):
            taster = Button(text=f"taster {i}")
            taster.size_hint = (None,None)
            root.add_widget(taster)

        return root


MyApp().run()