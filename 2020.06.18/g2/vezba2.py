import kivy
import kivy.properties
import kivy.event
import kivy.app
import kivy.uix.widget
import kivy.graphics
import kivy.core.window

kivy.core.window.Window.clearcolor = (1, 1, 1, 1)
kivy.core.window.Window.size = (800,600)

class Marko(kivy.app.App):
    def build(self):
        w = kivy.uix.widget.Widget()
        #w.background_color=(1,0,0,1)
        #instrukcija_boja = kivy.graphics.Color(1,0,1,1) 
        #w.canvas.add(instrukcija_boja)
        #w.canvas.add(instrukcija_crtez)
        instrukcija_boja = kivy.graphics.Color(1,0,0,1)
        instrukcija_crtez = kivy.graphics.Triangle(points=(100,100,100,200,200,100))
        w.canvas.add(instrukcija_boja)
        w.canvas.add(instrukcija_crtez)
        instrukcija_crtez.points[1] = 120
        instrukcija_crtez = kivy.graphics.Rectangle(pos=(400,400),size=(40,40))
        w.canvas.add(instrukcija_crtez)
        w.pos = (400,200)
        return w

aplikacija = Marko() 
aplikacija.run()


