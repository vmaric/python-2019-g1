import kivy
import kivy.properties
import kivy.event
import kivy.app
import kivy.uix.widget
import kivy.graphics
import kivy.core.window
import kivy.uix.button
import kivy.uix.boxlayout

kivy.core.window.Window.clearcolor = (1, 1, 1, 1)
kivy.core.window.Window.size = (800,600)

class Marko(kivy.app.App):
    def build(self):
        root = kivy.uix.boxlayout.BoxLayout(orientation="vertical")

        for i in range(10):
            taster = kivy.uix.button.Button(text="Hello People!") 
            root.add_widget(taster)

        donji = kivy.uix.boxlayout.BoxLayout()
        for i in range(5):
            taster = kivy.uix.button.Button(text="Hello People!")
            taster.size_hint = (1 if i != 2 else 0.5,1)
            donji.add_widget(taster)

        root.add_widget(donji)

        return root

aplikacija = Marko() 
aplikacija.run()


