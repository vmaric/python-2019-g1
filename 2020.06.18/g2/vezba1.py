import kivy
import kivy.properties
import kivy.event

class Proba(kivy.event.EventDispatcher):
    x = kivy.properties.NumericProperty()

class Proba1(kivy.event.EventDispatcher):
    y = kivy.properties.NumericProperty()
    
p = Proba()
p1 = Proba1()
p.bind(x=p1.setter("y"))

p.x = 30
print(p1.y)