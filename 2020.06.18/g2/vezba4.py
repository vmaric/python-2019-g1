import kivy
import kivy.properties
import kivy.event
import kivy.app
import kivy.uix.widget
import kivy.graphics
import kivy.core.window
import kivy.uix.button
import kivy.uix.boxlayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.pagelayout import PageLayout
from kivy.core.audio import SoundLoader
from kivy.uix.slider import Slider

kivy.core.window.Window.clearcolor = (1, 1, 1, 1)
kivy.core.window.Window.size = (800,600)

class Marko(kivy.app.App):
    def jump(self,sender):
        self.zvuk_jump.play()
    def build(self):
        self.zvuk = SoundLoader.load("c:/Users/Grupa1/Desktop/GAD/resources/back.wav")
        self.zvuk_jump = SoundLoader.load("c:/Users/Grupa1/Desktop/GAD/resources/jump.wav")
        root = PageLayout()

        self.zvuk.play()

        strana1 = kivy.uix.boxlayout.BoxLayout(orientation="vertical") 
        grid = GridLayout(rows=3,cols=3) 
        for i in range(9):
            taster = kivy.uix.button.Button(text=f"{i}")
            taster.bind(on_press=self.jump)
            grid.add_widget(taster) 
        strana1.add_widget(grid) 
        for i in range(10):
            taster = kivy.uix.button.Button(text="Hello People!") 
            taster.bind(on_press=self.jump)
            strana1.add_widget(taster)

        donji = kivy.uix.boxlayout.BoxLayout()
        for i in range(5):
            taster = kivy.uix.button.Button(text="Hello People!")
            taster.bind(on_press=self.jump)
            taster.size_hint = (1 if i != 2 else 0.5,1)
            donji.add_widget(taster) 
        strana1.add_widget(donji) 

        strana2 = kivy.uix.boxlayout.BoxLayout(orientation="vertical")
        slajder = Slider()
        slajder.min = 0
        slajder.max = 1
        slajder.bind(value=self.zvuk.setter("volume"))
        strana2.add_widget(slajder)
        strana2.add_widget(kivy.uix.button.Button(text="Druga strana",on_press=self.jump))



        strana3 = kivy.uix.button.Button(text="Treca strana",on_press=self.jump)

        root.add_widget(strana2)
        root.add_widget(strana3)

        root.add_widget(strana1)

        return root

aplikacija = Marko() 
aplikacija.run()


