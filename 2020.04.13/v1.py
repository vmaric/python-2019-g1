import http.server
import os

os.chdir("2020.04.13")

products = {
    1:{"naziv":"Televizor","cena":"Neprocenjivo"},
    2:{"naziv":"Telefon","cena":35.25},
    3:{"naziv":"Scott Scale","cena":250.55 }
}

class RequestHandler(http.server.SimpleHTTPRequestHandler):
    def write_headers(self):
        self.send_response(200)
        self.send_header("Content-type","text/html")
        self.end_headers() 
    def do_GET(self):
        path_parts = self.path.split("?")
        query = "" if len(path_parts)<2 else path_parts[1]
        print(path_parts[0])
        pr_key = 1
        if query:
            params = dict([x.split("=") for x in query.split("&")])
            if 'proizvod' in params:
                try:
                    pr_key = int(params['proizvod'])
                except:
                    pass 
        self.write_headers() 
        self.wfile.write(f"<h3>{products[pr_key]['naziv']}</h3>".encode("utf-8")) 
        self.wfile.write(f"<strong>Cena:</strong>{products[pr_key]['cena']}".encode("utf-8"))


server = http.server.HTTPServer(("localhost",80),RequestHandler)

server.serve_forever()