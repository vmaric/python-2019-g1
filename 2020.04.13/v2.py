import http.server
import urllib.parse
import os

os.chdir("2020.04.13") 

calc = {
    "add":lambda a,b: a+b,
    "sub":lambda a,b: a-b,
    "mul":lambda a,b: a*b,
    "div":lambda a,b: a/b
}

class RequestHandler(http.server.SimpleHTTPRequestHandler):
    def write_headers(self):
        self.send_response(200)
        self.send_header("Content-type","text/html")
        self.end_headers() 
    def do_GET(self):
        self.write_headers()
        url = urllib.parse.urlparse(self.path)
        params = urllib.parse.parse_qsl(url.query)
        params = dict(params)
        a = int(params["a"])
        b = int(params["b"])
        op = params["op"]
        res = calc[op](a,b)
        self.wfile.write(f"{res}".encode("utf-8"))


server = http.server.HTTPServer(("localhost",80),RequestHandler) 
server.serve_forever()