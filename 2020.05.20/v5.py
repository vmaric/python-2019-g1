import xml.etree.ElementTree as xml
class Hero:
    def __init__(self,name,role):
        self.name = name
        self.role = role
        self.health = 1000

    def __str__(self):
        return f"{self.name} {self.role}" 

h = Hero("Garen","Tank")

root = xml.Element("hero")
name = xml.SubElement(root,"name")
name.text = h.name
rola = xml.SubElement(root,"rola")
rola.text = h.role

#print(xml.tostring(root).decode("utf-8"))

xml.ElementTree(root).write("c:/wamp64/www/hero.xml")