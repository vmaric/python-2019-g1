import cv2
import time
import numpy as np
import copy 
from sklearn.svm import SVC

#Ucenje
dovlafaca = cv2.imread("dovlafaca.jpg")
dovlafaca = cv2.cvtColor(dovlafaca,cv2.COLOR_RGB2GRAY) 
dovlafaca = dovlafaca.reshape(68*68) 
vaynefaca = cv2.imread("vaynefaca.jpg")
vaynefaca = cv2.cvtColor(vaynefaca,cv2.COLOR_RGB2GRAY) 
vaynefaca = vaynefaca.reshape(68*68) 
mariafaca = cv2.imread("mariafaca.jpg")
mariafaca = cv2.cvtColor(mariafaca,cv2.COLOR_RGB2GRAY) 
mariafaca = mariafaca.reshape(68*68)  
model = SVC() 
model.fit([dovlafaca,vaynefaca,mariafaca],[0,1,2])  

klase = ("Dovla - ???","Vayne","Maria")

#Rad
sema = "c:/Users/admin/AppData/Local/Programs/Python/Python38/Lib/site-packages/cv2/data/haarcascade_frontalface_default.xml" 
klasifikator = cv2.CascadeClassifier(sema)

slika = cv2.imread("c:/Users/admin/Desktop/Vayne_1.jpg") 


cam = cv2.VideoCapture(0)

while True:
    _,slika = cam.read() 

    procslika = cv2.cvtColor(slika,cv2.COLOR_RGB2GRAY)   
    detektovano = klasifikator.detectMultiScale(procslika,1.3,10) 
    
    for x,y,w,h in detektovano: 
        #lice = detektovano.squeeze() 
        #x,y,w,h = lice
        licearr = procslika[y:y+h,x:x+w]
        licearr = cv2.resize(licearr,(68,68))
        licearr = licearr.reshape(68*68) 
        pred = model.predict([licearr])
        pred = klase[pred.squeeze()] 
        slika = cv2.rectangle(slika,(x,y),(x+w,y+h),(0,255,255),2) 
        slika = cv2.putText(slika,f"{pred}",(x,y+h+35),color=(0,255,255),fontFace=cv2.FONT_HERSHEY_DUPLEX,fontScale=1.2)

    cv2.imshow("Slika",slika)
    if cv2.waitKey(1) == 13:
        break


# red = 0
# korak = 1
# while True:
#     novaslika = copy.copy(slika)
#     linija = np.zeros(1215*3).reshape(1215,3)
#     print(linija.shape)
#     novaslika[red] = linija
#     cv2.imshow("Slika",novaslika)
#     red+=korak
#     if red==716 or red==-1:
#         korak*=-1

#     if cv2.waitKey(1) == 13:
#         break