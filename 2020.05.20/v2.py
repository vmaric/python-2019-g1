class Phone:
    def __init__(self,naziv,cena):
        self.naziv = naziv
        self.cena = cena
    def __str__(self):
        return f"{self.naziv}, {self.cena}"
    def serialize(self):
        return f"{self.naziv},{self.cena}" 
    @staticmethod
    def load():
        fajl = open("telefon","r")
        tel = Phone.unserialize(fajl.read())
        fajl.close()
        return tel

    @staticmethod
    def unserialize(txt):
        parts = txt.split(",")
        return Phone(parts[0],parts[1])
    def sacuvaj(self):
        fajl = open("telefon","w")
        fajl.write(self.serialize())
        fajl.close()

# p = Phone(input("Unesi naziv: "),input("Unesi cenu: ")) 
# p.sacuvaj()
# print(p)

p = Phone.load()
print(p)