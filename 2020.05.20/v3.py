import pickle

class Skin:
    def __init__(self,name,price):
        self.name = name
        self.price = price 
    def __str__(self):
        return f"{self.name} : {self.price}"  

# skin = Skin("Vayne Project",345)
 
# f = open("skin.dat","wb")
# pickle.dump(skin,f) 
# f.close()

# f = open("skin.dat","rb")
# data = f.read()
# deser = pickle.loads(data)
# print(type(deser))

skin = pickle.load(open("skin.dat","rb"))
print(skin)