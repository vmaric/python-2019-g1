import threading,time

notif = threading.Condition(threading.Lock())

def f():
    notif.acquire()
    print("hello")
    notif.wait()
    print("world")
    notif.release()

def f1():
    notif.acquire()
    print("hello 1")
    notif.wait()
    print("world 1")
    notif.release() 

nit1 = threading.Thread(None,f)
nit1.start() 
nit2 = threading.Thread(None,f1)
nit2.start() 
time.sleep(5)
print("Idemo dalje")
notif.acquire()
notif.notifyAll()
notif.release() 
