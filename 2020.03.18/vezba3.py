import threading
import time

numbers = []

locker = threading.Lock()

def f():
    locker.acquire()
    numbers.clear()
    for i in range(10):
        numbers.append(i)
    time.sleep(0.001)
    print(numbers)
    print(numbers[9])
    locker.release()
    time.sleep(0.001)
    locker.acquire()
    print("cao")
    locker.release()

def f1():
    locker.acquire()
    numbers.clear()
    for i in range(5):
        numbers.append(i)
    time.sleep(0.001)
    print(numbers)
    print(numbers[4])
    locker.release()
    

nit1 = threading.Thread(None,f)
nit2 = threading.Thread(None,f1)
nit1.start()
nit2.start()