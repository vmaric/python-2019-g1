import threading,time
import concurrent.futures

def f():
    print(threading.get_ident(),time.time())
    time.sleep(0.5)

pool = concurrent.futures.ThreadPoolExecutor()

for i in range(10000):
    pool.submit(f)