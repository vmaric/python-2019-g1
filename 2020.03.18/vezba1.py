import time
import _thread

def f():
    time.sleep(0.01)
    for i in range(100):
        print(f"Hello {i} man!")
        time.sleep(3)

def f1():
    time.sleep(0.01)
    for i in range(100):
        print(f"Hello from me {i} too!")
        time.sleep(1)

_thread.start_new_thread(f,())
_thread.start_new_thread(f1,())

while True:
    pass

