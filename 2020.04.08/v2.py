import http.server
import os 
import datetime 
import importlib  

os.chdir("2020.04.08/www") 

class MyServerApp(http.server.SimpleHTTPRequestHandler):
    pages = {
        "sobe":"rooms.html",
        "naslovna":"index.html",
        "kontakt":"contact.html",
        "hrana":"foods.html",
        "ronjenje":"dives.html",
        "vesti":"news.html",
        "onama":"about.html",
        "vlada":"vlada.html",
        "room.pys":"room"
    }
    def do_GET(self): 
        putanja = self.path.lstrip("/")
        print(putanja) 

        if not putanja:
            putanja = "naslovna"

        if putanja in self.pages:
            hdr = open("../strane/header.html","rb")
            self.wfile.write(hdr.read())

            if ".pys" in putanja:
                scr = importlib.import_module(f"scripts.{self.pages[putanja]}")
                scr.render(self)
            else:
                content = open(f"../strane/{self.pages[putanja]}","rb")
                self.wfile.write(content.read())
            
            ftr = open("../strane/footer.html","rb") 
            self.wfile.write(ftr.read())
        else:
            super().do_GET()

server = http.server.HTTPServer(("localhost",80),MyServerApp)

server.serve_forever()