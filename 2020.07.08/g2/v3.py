import xml.etree.ElementTree as tree
import urllib.request as request
import os
from flask import Flask

os.chdir("d:/python-2019-g1/2020.07.08/g2")
 


app = Flask(__name__)

@app.route("/")
def main(): 
    xmldoc = tree.parse(request.urlopen("http://localhost:5001/vesti")) # tree.parse("vesti.xml")
    root = xmldoc.getroot() 
    vesti = list(root.findall("vest")) 
    for vest in vesti:
        cena = tree.Element("cena")
        cena.text = "150"
        vest.append(cena)  
    return tree.tostring(root).decode(), 200, {"Content-Type":"application/xml"}


app.run(debug=True,port=5000)