from flask import Flask,request,render_template
import urllib.request as request
import os
import xml.sax as sax

os.chdir("d:/python-2019-g1/2020.07.08/g2")

app = Flask(__name__)

class XmlHandler(sax.ContentHandler):

    def __init__(self):
        self.vesti = []
        self.trenutna_vest = None
        self.trenutni_tag = ""

    def startDocument(self):
        print("Parsiranje")

    def startElement(self,naziv,atributi):
        if naziv == "vest":
            self.trenutna_vest = {"id":atributi['broj']} 
        else:
            if naziv != "vesti":
                self.trenutni_tag = naziv

    def endElement(self,naziv):
        if naziv == "vest":
            self.vesti.append(self.trenutna_vest)
            self.trenutna_vest = None
        self.trenutni_tag = ""
    
    def characters(self,sadrzaj):
        self.trenutna_vest[self.trenutni_tag] = sadrzaj

@app.route("/")
def naslovna(): 

    
    handler = XmlHandler()
    sax.parse("http://localhost:5001/vesti",handler)

    # res = request.urlopen("http://localhost:5001/vesti")
    # xml = res.read().decode()

    return render_template("vesti.html",**{"xml": "","marco":handler.vesti}) 

app.run(debug=True,port=5002)