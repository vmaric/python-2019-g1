import mysql.connector as conn
from flask import Flask 

app = Flask(__name__)
 
@app.route("/vesti")
def vesti():  
    db = conn.connect(host="localhost",database="vesti1",username="root",password="") 
    cur = db.cursor() 
    cur.execute("select * from vesti") 
    output = "<vesti>" 
    for vest in cur.fetchall():
        output+=f"<vest broj=\"{vest[0]}\">"
        output+=f"<naslov>{vest[1]}</naslov>"
        output+=f"<sadrzaj>{vest[2]}</sadrzaj>"
        output+=f"<vreme>{vest[3]}</vreme>"
        output+=f"<slika>{vest[4]}</slika>"
        output+="</vest>" 
    output += "</vesti>" 
    cur.close()
    db.close() 
    return output, 200, {"Content-type":"application/xml"}

app.run(port=5001,debug=True) 