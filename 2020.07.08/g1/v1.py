import xml.sax as sax
import os 
os.chdir("d:/python-2019-g1/2020.07.08/g1") 

class MojHedler(sax.handler.ContentHandler):
    def __init__(self):
        self.citanje = False
    def startDocument(self):
        #print("Pocinje parsiranje")
        pass

    def startElement(self,naziv,atributi):
        if naziv == "polaznik":
            self.citanje = True
            print(f"Polaznik {atributi['id']}")
            return
        if self.citanje:
            print(naziv,": ",end="")


    def characters(self,sadrzaj):
        if self.citanje and sadrzaj.strip():
            print(sadrzaj)

    def endElement(self,naziv):
        if naziv == "polaznik":
            self.citanje = False

    def endDocument(self):
        #print("Gotovo parsiranje")
        pass


sax.parseString(
    open("polaznici.xml","r").read(),
    MojHedler()
)