from baza import Db 

class Entitet:

    def azuriraj(self): 
        baza = Db.getconnection() 
        cur = baza.cursor() 
        polja = ",".join([f"{kljuc}='{self.__dict__[kljuc]}'" for kljuc in self.__dict__ if kljuc != self.__kljuc__])
        vrednost_kljuca = getattr(self,self.__kljuc__)
        cur.execute(f"update {self.__tabela__} set {polja} where {self.__kljuc__} = %s",(vrednost_kljuca,)) 
        baza.commit() 
        cur.close()   

    def snimi(self): 
        baza = Db.getconnection() 
        cur = baza.cursor()
        #{'id': 0, 'naziv': 'vladin motor', 'cena': 12345}
        polja = ",".join([f"{kljuc}='{self.__dict__[kljuc]}'" for kljuc in self.__dict__ if kljuc != self.__kljuc__])
        cur.execute(f"insert into {self.__tabela__} set {polja}") 
        baza.commit()
        setattr(self,self.__kljuc__,cur.lastrowid) 
        cur.close()  

    @classmethod
    def neki(klasa,filter=""): 
        rezultat = []
        baza = Db.getconnection()
        cur = baza.cursor() 
        cur.execute(f"select * from {klasa.__tabela__}{filter}") 
        svi = cur.fetchall()
        kolone = [naziv_kolone[0] for naziv_kolone in cur.description]
        for red in svi: 
            jedan = klasa() 
            for i,kolona in enumerate(kolone):
                setattr(jedan,kolona,red[i])
            rezultat.append(jedan)
        cur.close()   
        return rezultat
    @classmethod
    def jedan(klasa,_id): 
        rezultat = None
        baza = Db.getconnection()
        cur = baza.cursor() 
        cur.execute(f"select * from {klasa.__tabela__} where {klasa.__kljuc__} = %s limit 1",(_id,)) 
        red = cur.fetchone()
        if red:
            rezultat = klasa()
            kolone = [naziv_kolone[0] for naziv_kolone in cur.description]
            for i,kolona in enumerate(kolone):
                setattr(rezultat,kolona,red[i])
        cur.close()   
        return rezultat