from entitet import Entitet 
class Motor(Entitet):  
    __tabela__  = "motori"
    __kljuc__   = "id"

    def __init__(self):
        self.id = 0
        self.naziv = ""
        self.cena = ""    
    def render(self):
        color = "red" if self.cena > 1000 else "green"
        return f"<div style='border:1px solid {color};padding:4px;margin:4px;'>{self.naziv} {self.cena}</div>"  