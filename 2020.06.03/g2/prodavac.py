import mysql.connector as connector

class Prodavac: 
    def __init__(self,pid,ime,prezime):
        self.id         = pid
        self.ime        = ime
        self.prezime    = prezime

    @staticmethod
    def jedanprodavac(idprodavca): 
        rezultat = None
        baza = connector.connect(host="localhost",database="sakila",username="root",passwd="") 
        cur = baza.cursor() 
        cur.execute("select * from prodavci where id = %s limit 1",(idprodavca,)) 
        red = cur.fetchone()
        if red:
            rezultat = Prodavac(red[0],red[1],red[2]) 
        cur.close()  
        baza.close() 
        return rezultat

    @staticmethod
    def listaprodavaca():
        rezultat = []
        baza = connector.connect(host="localhost",database="sakila",username="root",passwd="") 
        cur = baza.cursor() 
        cur.execute("select * from prodavci") 
        for motor in cur.fetchall():
            m = Prodavac(motor[0],motor[1],motor[2]) 
            rezultat.append(m)
        cur.close()  
        baza.close() 
        return rezultat
    
    def snimi(self): 
        baza = connector.connect(host="localhost",database="sakila",username="root",passwd="") 
        cur = baza.cursor() 
        cur.execute("insert into prodavci values (null,%s,%s)",(self.ime,self.prezime)) 
        baza.commit()
        self.id = cur.lastrowid
        cur.close()  
        baza.close()  

    def azuriraj(self): 
        baza = connector.connect(host="localhost",database="sakila",username="root",passwd="") 
        cur = baza.cursor() 
        cur.execute("update prodavci set ime=%s,prezime=%s where id = %s",(self.ime,self.prezime,self.id)) 
        baza.commit() 
        cur.close()  
        baza.close() 
    
    def render(self): 
        return f"<div style='border:1px solid {color};padding:4px;margin:4px;'><strong>{self.ime} {self.prezime}</strong></div>"  