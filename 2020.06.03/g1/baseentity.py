from db import Db
class BaseEntity:

    def update(self):
        db = Db.getconnection()
        cur = db.cursor()
        parametri = ",".join([f"{kk}='{self.__dict__[kk]}'" for kk in self.__dict__ if kk != self.__keycolumn__])
        idval = getattr(self,self.__keycolumn__)
        upit = f"update {self.__tabela__} set {parametri} where {self.__keycolumn__} = {idval}"
        cur.execute(upit)
        db.commit()
        cur.close()

    def save(self):
        db = Db.getconnection()
        cur = db.cursor() 
        parametri = ",".join([f"{kk}='{self.__dict__[kk]}'" for kk in self.__dict__ if kk != self.__keycolumn__])
        upit = f"insert into {self.__tabela__} set {parametri}"
        cur.execute(upit)
        setattr(self,self.__keycolumn__,cur.lastrowid) 
        db.commit()
        cur.close()  

    @classmethod
    def get(klasa,gid):
        db = Db.getconnection()
        cur = db.cursor() 
        cur.execute(f"select * from {klasa.__tabela__} where {klasa.__keycolumn__} = {gid} limit 1")
        red = cur.fetchone()
        kolone = [kolona[0] for kolona in cur.description]  
        cur.close()
        noviobjekat = klasa() 
        for i,k in enumerate(red):
            setattr(noviobjekat,kolone[i],k) 
        return noviobjekat

    @classmethod
    def getall(klasa):   
        db = Db.getconnection()
        cur = db.cursor() 
        cur.execute(f"select * from {klasa.__tabela__}")
        kolone = [kolona[0] for kolona in cur.description]  
        svi = cur.fetchall()
        sviglumci = [] 
        for red in svi:
            noviobjekat = klasa()
            for i,k in enumerate(red):
                setattr(noviobjekat,kolone[i],k) 
            sviglumci.append(noviobjekat)
        cur.close()
        return sviglumci

    def delete(self):
        db = Db.getconnection()
        cur = db.cursor()
        keyvalue = getattr(self,self.__keycolumn__)
        cur.execute(f"delete from {self.__tabela__} where {self.__keycolumn__}=%s",(keyvalue,))
        db.commit()
        cur.close()