import sqlalchemy.ext

engine = sqlalchemy.create_engine(
    "mysql+mysqlconnector://root:@localhost/sakila",
    pool_size=10
)

meta = sqlalchemy.MetaData(engine) 

users = sqlalchemy.Table(
    "users",meta,
    autoload=True
)

res = engine.execute(users.select().where(users.c.id==1))

for r in res.fetchall():
    print(r.username)


# tabela = sqlalchemy.Table(
#     "users",meta,
#     sqlalchemy.Column("id",sqlalchemy.Integer(),primary_key=True),
#     sqlalchemy.Column("username",sqlalchemy.String(256)),
#     sqlalchemy.Column("password",sqlalchemy.String(50)),
# )

# engine.execute(tabela.update().values(username="Vlada",password="3456",id=1))

# tabela.drop()
# tabela.create()
