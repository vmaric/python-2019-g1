from baseentity import BaseEntity

class Korisnik(BaseEntity):
    __tabela__ = "korisnik"
    __keycolumn__ = "id"
    def __init__(self):
        self.id = 0
        self.ime = ""
        self.prezime = ""