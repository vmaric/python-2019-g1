import mysql.connector as conn 
class Db:
    __baza = None
    @staticmethod
    def getconnection():
        if not Db.__baza:
            Db.__baza = conn.connect(host="localhost",database="sakila",user="root",passwd="")
        return Db.__baza