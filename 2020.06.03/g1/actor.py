from db import Db
from baseentity import BaseEntity

class Actor(BaseEntity):
    __tabela__ = "actor"
    __keycolumn__ = "actor_id"
    def __init__(self):
        self.actor_id = 0
        self.first_name = ""
        self.last_name = ""   

    def showname(self):
        return f"{self.first_name} {self.last_name}"
    
    def show(self):
        return f"<div style='border:1px solid red;padding:4px;margin:4px;'>{self.showname()}</div>"

    def __str__(self):
        return "cao"