from db import Db
from baseentity import BaseEntity

class Film(BaseEntity):
    __tabela__ = "film"
    __keycolumn__ = "film_id"
    def __init__(self):
        self.film_id = 0
        self.title = ""   
 
    
    def show(self):
        return f"<div style='border:1px solid red;padding:4px;margin:4px;'>{self.title}</div>"