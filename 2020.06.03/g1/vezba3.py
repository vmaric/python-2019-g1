import sqlalchemy.ext.declarative 

BaseEntity = sqlalchemy.ext.declarative.declarative_base()

class Actor(BaseEntity):
    __tablename__ = "actor"
    actor_id = sqlalchemy.Column("actor_id",sqlalchemy.Integer(),primary_key=True,autoincrement=True)
    first_name = sqlalchemy.Column("first_name",sqlalchemy.String(256))
    last_name = sqlalchemy.Column("last_name",sqlalchemy.String(256)) 
    def show(self):
        print(f"Hello {self.first_name} {self.last_name}") 

actor = Actor()
actor.save()
actor.show()