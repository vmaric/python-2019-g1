from ctrl.basecontroller import basecontroller
import model.repo
import datetime

class sitecontroller(basecontroller):
    def index(self,req,params):
        vreme = datetime.datetime.now()
        
        repository = model.repo.repo()

        proizvodi = repository.getproducts()

        prizvodi_content = ""
        for proizvod in proizvodi:
            view = self.load_view("article",proizvod)
            prizvodi_content+=view
 
        view = self.load_view("jela",{"vreme":vreme,"proivodi":prizvodi_content})
        req.wfile.write(view.encode())