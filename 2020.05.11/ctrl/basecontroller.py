class basecontroller:

    cache = {}

    def load_view(self,view,params = {}):
        if view in basecontroller.cache:
            view_content = basecontroller.cache[view]
            print(f"View {view} taken from cache")
        else:
            view_file = open(f"view/{view}.tpl.html","r")
            view_content = view_file.read()
            basecontroller.cache[view] = view_content
        for p in params:
            kljuc = "{"+p+"}" 
            view_content = view_content.replace(kljuc,str(params[p]))
        return view_content

    def index(self,req,params):
        req.wfile.write(b"No implementation")