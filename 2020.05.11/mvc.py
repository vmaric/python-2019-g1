import http.server
import sys
import importlib
import urllib.parse as parse
import os


os.chdir("2020.05.11")

class handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        if(self.path.startswith("/static")):
            super().do_GET()
        else:
            self.send_response(200)
            self.send_header("content-type",'text/html')
            self.end_headers()
            url = parse.urlparse(self.path)
            parametri = dict(parse.parse_qsl(url.query)) 
            delovi_putanje = self.path.replace(f"?{url.query}","").lstrip("/").split("/")
            trazeni_kontroler = "site"
            trazeni_metod = "index"
            try:
                if delovi_putanje[0]:
                    trazeni_kontroler = delovi_putanje[0]
                if delovi_putanje[1]:
                    trazeni_metod = delovi_putanje[1] 
            except:
                pass  
            trazeni_kontroler += "controller" 
            modul_kontrolera = importlib.import_module(f"ctrl.{trazeni_kontroler}") 
            klasa_objekat = getattr(modul_kontrolera,trazeni_kontroler) 
            kontroler_objekat = klasa_objekat() 
            metod = getattr(kontroler_objekat,trazeni_metod)
            metod(self,parametri)  
def run(): 
    http.server.HTTPServer(("localhost",80),handler).serve_forever() 

run()