import mysql.connector as conn
from predavac import Predavac

class Repo: 
    __db = None
    @staticmethod
    def connect():
        if not Repo.__db:
            Repo.__db = conn.connect(
                host="localhost",database="casovi",user="root",passwd="")
        return Repo.__db
    
    @staticmethod
    def insertPredavac(predavac):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute("insert into predavaci values (null,%s,%s,%s)",(predavac.ime,predavac.prezime,predavac.predmet)) 
        predavac.id = cur.lastrowid 
        db.commit()
        cur.close()

    @staticmethod
    def updatePredavac(predavac):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute("update predavaci set ime=%s,prezime=%s,predmet=%s where id = %s",(predavac.ime,predavac.prezime,predavac.predmet,predavac.id))  
        db.commit()
        cur.close()

    @staticmethod
    def deletePredavac(pid):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute("delete from predavaci where id = %s",(pid,))  
        db.commit()
        cur.close()

    @staticmethod
    def getPredavac(pid):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute("select * from predavaci where id = %s",(pid,))
        podaci = cur.fetchone()
        res = None
        if podaci: 
            res = Predavac(podaci[0],podaci[1],podaci[2],podaci[3]) 
        cur.close()
        return res

    @staticmethod
    def getPredavaci(filter=""):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute(f"select * from predavaci{filter}")
        podaci = cur.fetchall()
        res = []
        for p in podaci:
            res.append(Predavac(p[0],p[1],p[2],p[3]))
        cur.close()
        return res





