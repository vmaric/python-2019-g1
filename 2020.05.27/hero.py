import time,random
class Hero:
    def add_subscriber(self,subscriber):
        self.subscribers.append(subscriber)
    def __init__(self):
        self.health = 100
        self.subscribers = []
    def hit(self):
        self.health -= 2
        for s in self.subscribers:
            s.onhit(self.health)
    def play(self):
        j = random.randint(2,9)
        print("Hero is playing")
        for i in range(10):
            if j == i:
                self.hit()
                #print("Hero has been hit")
            print(f"Round {i}")
            time.sleep(0.5)