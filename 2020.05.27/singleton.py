class Person:
    __instance = None 
    @staticmethod
    def getPerson():
        if not Person.__instance:
            Person.__instance = Person
        return Person.__instance 
    def __init__(self):
        self.ime = None
        if not Person.__instance:
            Person.__instance = self
        else:
            raise Exception("Cannot instantiate") 

p = Person()
p.ime = "Peter"
p1 = Person.getPerson()
p1.ime = "Sally" 
print(p1.ime)

