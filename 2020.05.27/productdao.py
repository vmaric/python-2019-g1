from product import Product
from repo import Repo

class ProductDAO:
    __instance = None
    @staticmethod
    def getinstance():
        if not ProductDAO.__instance:
            ProductDAO.__instance = ProductDAO()
        return ProductDAO.__instance

    def insert(self,pr):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute("insert into proizvodi values (null,%s,%s)",(pr.naziv,pr.cena)) 
        pr.id = cur.lastrowid 
        db.commit()
        cur.close() 

    def update(self,pr):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute("update proizvodi set naziv=%s,cena=%s where id = %s",(pr.naziv,pr.cena,pr.id))  
        db.commit()
        cur.close()
 
    def delete(self,pid):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute("delete from proizvodi where id = %s",(pid,))  
        db.commit()
        cur.close()
 
    def getone(self,pid):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute("select * from proizvodi where id = %s",(pid,))
        podaci = cur.fetchone()
        res = None
        if podaci: 
            pr = Product()
            pr.id = podaci[0]
            pr.naziv = podaci[1]
            pr.cena = podaci[2]
            res = pr 
        cur.close()
        return res
 
    def getmany(self,filter=""):
        db = Repo.connect()
        cur = db.cursor()
        cur.execute(f"select * from proizvodi{filter}")
        podaci = cur.fetchall()
        res = []
        for p in podaci:
            pr = Product()
            pr.id = p[0]
            pr.naziv = p[1]
            pr.cena = p[2]
            res.append(pr)
        cur.close()
        return res