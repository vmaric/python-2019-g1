class Predavac:
    def __init__(self,id=None,ime=None,prezime=None,predmet=None):
        self.id = id
        self.ime = ime
        self.prezime = prezime
        self.predmet = predmet
    def __str__(self):
        return f"{self.id} {self.ime} {self.prezime} {self.predmet}"