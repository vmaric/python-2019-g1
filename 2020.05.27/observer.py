import hero 

class semafor:
    def onhit(self,h):
        print(f"Hero was hit. {h} health left")
class prevod:
    def onhit(self,h):
        print(f"Opasan udarac. Ostalo samo {h} zdravlja")
 
h = hero.Hero()
h.add_subscriber(semafor())
h.add_subscriber(prevod())
h.play()