class Hero:
    def __init__(self):
        self.armour = ""
        self.weapon = ""
        self.helmet = ""
    def show(self):
        print(self.armour,self.weapon,self.helmet)

class HeroBuilder:
    def __init__(self):
        self.hero = Hero()
    def applyArmor(self,armour):
        self.hero.armour = armour
        return self
    def applyWeapon(self,weapon):
        self.hero.weapon = weapon
        return self
    def applyHelmet(self,helmet):
        self.hero.helmet = helmet
        return self
    def build(self):
        return self.hero

hero = HeroBuilder().applyArmor("Chest Plate").applyHelmet("Golden helmet").applyWeapon("B.F. Sword").build()

hero.show()