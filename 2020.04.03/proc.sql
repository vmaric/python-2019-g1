delimiter #
drop procedure if exists usp_register#
create procedure usp_register(
	uname varchar(256),
    passwd varchar(50)
)
begin

    declare ex int;
    
    select count(*) from users 
    where username = uname into ex; 
    
    if ex then
		select 0;
	else
		insert into users values (null,uname,md5(passwd));
        select 1;
	end if;
end#