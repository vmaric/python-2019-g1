import mysql.connector as conn

db = conn.connect(
    host="localhost",
    database="sakila",
    username="root",
    passwd=""
)

pretraga = input("Unesi film:")

cur = db.cursor()
cur.execute(
    "select film_id,title from film where description like %s",
    ("%"+pretraga+"%",))
res = cur.fetchall()
for r in res:
    print(r)

db.close()