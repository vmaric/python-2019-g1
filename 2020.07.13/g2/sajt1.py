from flask import Flask,request
import urllib.request as req
import xml.dom.minidom as domxml  
import barcode
from barcode.writer import ImageWriter  
import os


os.chdir("d:/python-2019-g1/2020.07.13/g2")

app = Flask(__name__) 

def getribice():
    res = req.urlopen("http://localhost:5000/")
    doc = domxml.parse(res)
    sveribice = doc.getElementsByTagName("ribica")
    ribice_res = []
    for jednaribica in sveribice:
        rid = jednaribica.getAttribute("id")

        ean = barcode.get('code128', rid, writer=ImageWriter()) 
        ean.default_writer_options['text_distance']=0
        ean.save(f"static/product_{rid}") 

        naziv = jednaribica.childNodes[0].childNodes[0].nodeValue
        slika = jednaribica.getElementsByTagName("slika")[0].childNodes[0].nodeValue
        stanje = jednaribica.childNodes[2].childNodes[0].nodeValue
        cena = jednaribica.childNodes[3].childNodes[0].nodeValue
        ribice_res.append((rid,naziv,slika,stanje,cena))
    return ribice_res

@app.route("/")
def ribice():
    r = getribice()
    izlaz = "<script>var ws = new WebSocket('ws://localhost:8765');ws.onmessage=function(data){ var d = data.data.split(','); document.getElementById('stanje_'+d[0]).innerHTML = d[1] }</script>"
    for ribica in r:
        izlaz+="<div style='position:relative; border:1px solid red;padding:4px;margin:4px;'>"
        izlaz+=ribica[0]+" "+ribica[1] +"<br><img width=100 src='"+ribica[2]+"' /><br>"
        izlaz+="Cena: " + ribica[4] + "<br>"
        izlaz+="Stanje: <span id='stanje_"+ribica[0]+"'>" + ribica[3] + "</span><br>"
        izlaz+="<img style='position:absolute;top:0px;right:0px;' width=100 src='static/product_"+ribica[0]+".png' /><br>"
        izlaz+="</div>"
    return izlaz

app.run(debug=True,port=80)