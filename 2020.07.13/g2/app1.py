import mysql.connector as conn
import xml.dom.minidom as domxml
from flask import Flask,request

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="ribice")   

def generateXml():
    db = getconnection()
    cur = db.cursor()
    cur.execute("select * from ribice")  
    doc = domxml.Document()
    ribice = doc.createElement("ribice") 
    doc.appendChild(ribice)  
    sve_ribice = cur.fetchall()
    for ribicadb in sve_ribice: 
        ribica = doc.createElement("ribica") 
        ribica.setAttribute("id",str(ribicadb[0]))
        kolone = ("naziv","slika","stanje","cena")
        for i,kolona in enumerate(kolone):
            naziv_taga = doc.createElement(kolona)  
            ribica.appendChild(naziv_taga)
            tekst_taga = doc.createTextNode(str(ribicadb[i+1]))
            naziv_taga.appendChild(tekst_taga) 
        ribice.appendChild(ribica)  
    db.close() 
    return doc.toxml()

app = Flask(__name__)

@app.route("/")
def home():
    return generateXml(),200,{"Content-Type":"application/xml"}

app.run(debug=True)



