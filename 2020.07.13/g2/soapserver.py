import mysql.connector as conn 
import logging
logging.basicConfig(level=logging.ERROR)
from spyne import Application, rpc, ServiceBase, Integer32, Unicode
from spyne import Iterable
from spyne.protocol.soap import Soap12
from spyne.server.wsgi import WsgiApplication 
import asyncio
import websockets
import threading

korisnici = {
    
}

async def echo(websocket, path):
    korisnici[websocket] = websocket
    try:
        while True:
            poruka = await websocket.recv()
            print(poruka)
    except:
        del korisnici[websocket]

async def broadcast(rid,cnt):
    for korisnik in korisnici:
        await korisnik.send(f"{rid},{cnt}")
 
def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="ribice")   

class KupovinaServis(ServiceBase):
    @rpc(Integer32,_returns=Integer32)
    def kupi(self,rid):
        db = getconnection()
        cur = db.cursor()
        cur.execute(f"update ribice set stanje = stanje - 1 where id = {rid}")
        db.commit()
        cur.execute(f"select stanje from ribice where id = {rid}")
        res = cur.fetchone()
        cur.close()
        db.close()

        asyncio.run(broadcast(rid,res[0]))
           

        return res[0] 
 
application = Application([KupovinaServis],
    tns='com.prodavnicaribica',
    in_protocol=Soap12(validator='lxml'),
    out_protocol=Soap12()
)
# if __name__ == '__main__': 
#     from wsgiref.simple_server import make_server
#     wsgi_app = WsgiApplication(application)
#     server = make_server('0.0.0.0', 8000, wsgi_app)
#     server.serve_forever()

class SoapServer(threading.Thread):
    def run(self): 
        from wsgiref.simple_server import make_server
        wsgi_app = WsgiApplication(application)
        server = make_server('0.0.0.0', 8000, wsgi_app)
        server.serve_forever()

SoapServer().start()

asyncio.get_event_loop().run_until_complete(
    websockets.serve(echo, 'localhost', 8765))
asyncio.get_event_loop().run_forever()