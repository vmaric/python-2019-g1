from flask import Flask,request,redirect,url_for
import urllib.request as req
import xml.dom.minidom as xmldom


app = Flask(__name__)

@app.route("/")
def naslovna():
    res = req.urlopen("http://127.0.0.1:5000/products.xml")
    doc = xmldom.parse(res)
    proizvodi = doc.getElementsByTagName("product")
    izlaz = ""
    for product in proizvodi:
        pid = product.getAttribute("id")
        naziv = product.getElementsByTagName("naziv")[0].childNodes[0].nodeValue
        cena = product.getElementsByTagName("cena")[0].childNodes[0].nodeValue
        cena = float(cena)*1.5
        kolicina = product.getElementsByTagName("kolicina")[0].childNodes[0].nodeValue
        nastanju = int(kolicina) > 0
        izlaz+=f"<div style='border:1px solid red;padding:4px;margin:4px;'>{naziv}, cena: {cena}, na stanju: {kolicina}, <a href='kupovina/{pid}'>Kupi</a></div>"
    return izlaz

@app.route("/kupovina/<int:pid>")
def kupovina(pid):
    r = req.Request(f"http://127.0.0.1:5000/api",f"<metod><naziv>kupi</naziv><arg>{pid}</arg></metod>".encode())
    r.method="POST"
    r.add_header("Content-Type","application/xml")
    res = req.urlopen(r)
    res = res.read().decode()
    print(res) 
    return redirect(url_for("naslovna"))

app.run(debug=True,port=8000)