import mysql.connector as conn
import xml.dom.minidom as domxml
from flask import Flask, request
import os
import logging
logging.basicConfig(level=logging.DEBUG)
from spyne import Application, rpc, ServiceBase, Integer, Unicode, ComplexModel
from spyne import Iterable
from spyne.protocol.soap import Soap12
from spyne.server.wsgi import WsgiApplication

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="shop2")

class Proizvod(ComplexModel):
    pid = Integer()
    naziv = Unicode()

class Prodavnica(ServiceBase):
    @rpc(_returns=Iterable(Proizvod))
    def proizvodi(self):
        db = getconnection()
        cur = db.cursor()
        cur.execute("select * from products")
        res = []
        for pid,naziv,cena,kolicina in cur.fetchall(): 
            p = Proizvod()
            p.pid = pid
            p.naziv = naziv
            res.append(p)
        cur.close()
        db.close()
        return res

    @rpc(Integer, _returns=Integer)
    def kupi(self,pid):
        db = getconnection()
        cur = db.cursor()
        cur.execute("update products set kolicina = kolicina - 1 where id = %s",(pid,))
        db.commit()
        cur.close()
        db.close()  
        return 1 

application = Application([Prodavnica],
    tns='com.prodavnica',
    in_protocol=Soap12(validator='lxml'),
    out_protocol=Soap12()
)
if __name__ == '__main__': 
    from wsgiref.simple_server import make_server
    wsgi_app = WsgiApplication(application)
    server = make_server('0.0.0.0', 8000, wsgi_app)
    server.serve_forever()

