import mysql.connector as conn 
from flask import Flask,request

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="shop2") 

db = getconnection()
cur = db.cursor()
cur.execute("select * from products") 
print(cur.fetchall()) 
db.close() 

app = Flask(__name__)

@app.route("/proizvod",methods=["GET","POST","PUT","DELETE"])
def proizvod_api():
    metod = request.method.lower()
    if metod == "get":
        return "Poslao si get zahtev"
    elif metod == "post":
        return "Poslao si post zahtev"
    elif metod == "put":
        return request.data
    elif metod == "delete":
        return "Poslao si delete zahtev"

app.run(debug=True)
