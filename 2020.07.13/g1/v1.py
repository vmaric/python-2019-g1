import mysql.connector as conn
import xml.dom.minidom as domxml
from flask import Flask, request
import os

os.chdir("d:/python-2019-g1/2020.07.13/g1")
 
app = Flask(__name__) 

def getconnection():
    return conn.connect(host="localhost",username="root",passwd="",database="shop2")

def kupi(pid):
    db = getconnection()
    cur = db.cursor()
    cur.execute("update products set kolicina = kolicina - 1 where id = %s",(pid,))
    db.commit()
    cur.close()
    db.close()

@app.route("/products.xml")
def products():
    doc = domxml.Document() 
    root = doc.createElement("products")  
    db = getconnection()
    cur = db.cursor()
    cur.execute("select * from products")
    for pr in cur.fetchall(): 
        proizvod = doc.createElement("product") 
        proizvod.setAttribute("id",str(pr[0]))
        nazivi = ["naziv","cena","kolicina"]
        for i,kolona in enumerate(nazivi):
            k = doc.createElement(kolona)
            k_txt = doc.createTextNode(str(pr[i+1]))
            k.appendChild(k_txt) 
            proizvod.appendChild(k)
        root.appendChild(proizvod)  
    db.close()   
    doc.appendChild(root) 
    return doc.toxml()

metodi = {
    "kupi":kupi    
}
 
@app.route("/api",methods=["POST"])
def kupovina():
    telo = request.data
    dom = domxml.parseString(telo.decode())
    metod = dom.getElementsByTagName("naziv")[0].childNodes[0].nodeValue
    arg = dom.getElementsByTagName("arg")[0].childNodes[0].nodeValue 
    print(metod,arg)
    metodi[metod](arg)
    return "1"

app.run(debug=True)

