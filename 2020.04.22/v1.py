import numpy

person = [('id',numpy.int32),('age',numpy.int8),('balance',numpy.float32)]

arr = numpy.array(
    [
        (10,25,125),
        (20,45,225)
    ],
    dtype=person
)

print(arr)

