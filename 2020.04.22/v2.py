import numpy as np
import matplotlib.pyplot as plt 

figura = plt.figure(figsize=(5,5),facecolor="cyan")
figura.suptitle("Moj prvi grafikon")

osa  = figura.add_subplot(232)
osa1 = figura.add_subplot(555)
osa2 = figura.add_subplot(251)
osa3 = figura.add_subplot(5,5,10) 
osa4 = figura.add_subplot(212)


osa2.plot([100,200,300,400])
osa1.plot(np.random.random(10))
osa3.plot(np.random.random(10))
# osa.plot(np.random.random(10))


osa4.plot(
    [19,20,21,22],[305,350,320,250],"--og",
    label="oboleli"
) 
osa4.plot( 
    [19,20,21,22],[100,200,300,400],"--*r",
    label="ozdravili"
)
osa4.legend()
# osa4.ylabel("Broj obolelih")
# osa4.xlabel("Datumi")

osa4.bar(
    [19,20,21,22],[305,350,320,250],width=0.3,color="r"
)
osa4.bar(
    [19.3,20.3,21.3,22.3],[250,320,305,230],width=0.3,color="g"
)
osa4.bar(
    [19.6,20.6,21.6,22.6],[260,330,315,240],width=0.3,color="y"
)



# osa = figura.add_axes((0.1,0.1,0.8,0.35)) 
# osa = figura.add_axes((0.1,0.55,0.8,0.35)) 

plt.show()