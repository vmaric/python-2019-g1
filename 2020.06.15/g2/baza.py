import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker

engine = sqlalchemy.create_engine("mysql+mysqlconnector://root:@localhost/sakila")  
Entity = declarative_base() 
Session = sessionmaker(engine)

class player(Entity):
    __tablename__ = "players"
    pid = sqlalchemy.Column("id",sqlalchemy.Integer,primary_key=True)
    username = sqlalchemy.Column(sqlalchemy.String(256))
    passwd = sqlalchemy.Column(sqlalchemy.String(256))
    level = sqlalchemy.Column(sqlalchemy.Integer)
    balance = sqlalchemy.Column(sqlalchemy.Float)
    ptype = sqlalchemy.Column("type",sqlalchemy.String(50))
    px = sqlalchemy.Column(sqlalchemy.Integer)
    py = sqlalchemy.Column(sqlalchemy.Integer)




# p = player()
# p.username = "dovla"
# p.passwd = "123"
# p.ptype = "paladin"
# p.px = 50
# p.py = 60
# p.balance = 125.35

# sesija.add(p)
# sesija.commit()

def login(un,pwd): 
    sesija = Session()
    upit = sesija.query(player)
    upit = upit.filter(player.username == un , player.passwd == pwd) 
    if upit.count() == 1:
        res = upit.one()
    else:
        res = None
    sesija.close()
    return res