import tkinter 
import baza  
import importlib
import sys

prozor = tkinter.Tk()
prozor.geometry("600x400") 

logged_user = None

otvoren_prozor = None
def otvori(nazivprozora): 
    global otvoren_prozor
    if otvoren_prozor:
        otvoren_prozor.dispose()
        otvoren_prozor.forget()
    prozor_klasa = importlib.import_module(nazivprozora)
    kontejner = prozor_klasa.Prozor()
    kontejner.main_app = sys.modules[__name__]
    kontejner.setprozor()
    kontejner.pack(expand=True,fill="both") 
    otvoren_prozor = kontejner
    
logged_user = baza.login("dovla","123")
otvori("gameplay")

prozor.mainloop()