import tkinter
from tkinter.ttk import *
import tkinter.messagebox as msgb
import baza

class Prozor(tkinter.Frame):
    def dispose(self):
        pass
    def login(self):
        print("Login button pressed")
        print(self.un_var.get(),self.pwd_var.get())
        userok = baza.login(self.un_var.get(),self.pwd_var.get())
        if userok:
            msgb.showinfo("Dobro dos'o",f"Dobro nam dos'o {self.un_var.get()}")
            self.main_app.logged_user = userok
            self.main_app.otvori("gameplay")
        else:
            msgb.showwarning("Dobro nam nedos'o","Zalicemo se!")
    def setprozor(self):  
        self.un_var = tkinter.StringVar(self)
        self.pwd_var = tkinter.StringVar(self) 
        en_username = Entry(self,textvariable=self.un_var)
        en_username.pack()
        en_password = Entry(self,textvariable=self.pwd_var)
        en_password.pack()
        btn_login = Button(self,text="Login",command=self.login)
        btn_login.pack() 