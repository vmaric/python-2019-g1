import tkinter
import tkinter.messagebox as mbox
import tkinter.simpledialog as sd 
prozor = tkinter.Tk()

godina = sd.askinteger("Godine?","How old are you?",parent=prozor) 

#odgovor = mbox.askquestion("Godine...","Are you over 13?")
#print(odgovor) 
odgovor = godina > 13
if odgovor == False:
    mbox.showinfo("Access denied","You must be over 13",parent=prozor)
else:
    mbox.showinfo("Access granted","You can access, welcome to our amazing program")

#prozor.mainloop()