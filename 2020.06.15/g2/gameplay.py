import tkinter
from tkinter.ttk import *
import tkinter.messagebox as msgb
import baza,time

class Prozor(tkinter.Frame): 

    def dispose(self):
        pass
    def update(self):
        self.after(16,self.update)

        if 37 in self.tasteri:
            self.px -= self.speed
        if 39 in self.tasteri:
            self.px += self.speed
        if 38 in self.tasteri:
            self.py -= self.speed
        if 40 in self.tasteri:
            self.py += self.speed

        self.lbl.place(x=self.px,y=self.py)
        self.cnv.moveto(self.igrac,self.px,self.py)
        #print(time.time())
    def ku(self,evt):
        if evt.keycode in self.tasteri:
            del self.tasteri[evt.keycode]
    def kd(self,evt):
        self.tasteri[evt.keycode] = True
        # if evt.keycode==37:
        #     self.px-=self.speed
        # if evt.keycode==39:
        #     self.px+=self.speed 
    def setprozor(self): 
        self.tasteri = {}
        self.speed = 3
        self.bind("<KeyPress>",self.kd)
        self.bind("<KeyRelease>",self.ku)
        self.focus()
        self.px = self.main_app.logged_user.px
        self.py = self.main_app.logged_user.px
        self.cnv = tkinter.Canvas(self,bg="yellow",highlightthickness=0)
        self.cnv.pack(expand=True,fill="both")
        self.igrac = self.cnv.create_rectangle(self.px,self.py,self.px+50,self.py+50,width=0,fill="red")
        self.lbl = Label(self,text=self.main_app.logged_user.username)
        self.lbl.place(x=self.px,y=self.py)
        self.after(16,self.update)