import tkinter
import sys
import importlib

win = tkinter.Tk()
win.geometry("600x400")
currentpage = None
def switchpage(page): 
    global currentpage
    if currentpage:
        currentpage.dispose()
        currentpage.forget()
    modul = importlib.import_module(f"pages.{page}") 
    fr = modul.AppFrame(win)
    fr.setcontrols(sys.modules[__name__])
    fr.pack(expand=True,fill="both")
    currentpage = fr

switchpage("svi")

win.mainloop()