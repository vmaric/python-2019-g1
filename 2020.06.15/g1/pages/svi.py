import tkinter
from tkinter.ttk import *
import model

class AppFrame(tkinter.Frame): 
    def azuriraj(self):
        print(self.lb_svi.curselection())
    def odabran(self,evt):
        ind = self.lb_svi.curselection()[0]
        print(self.podaci[ind])
    def dispose(self):
        pass
    
    def setcontrols(self,parentmodule):  
        f = tkinter.Frame(self)
        sb = tkinter.Scrollbar(f)
        sb.pack(anchor="ne",fill="y",side=tkinter.RIGHT)
        self.lb_svi = tkinter.Listbox(f)
        self.lb_svi.pack(anchor="ne",fill="y",side=tkinter.RIGHT) 
        sb.config(command=self.lb_svi.yview)
        f.pack()
        self.podaci = model.getusers()
        for i,podatak in enumerate(self.podaci):
            self.lb_svi.insert(i,podatak)
        azuriranje = Button(self,text="Azuriraj",command=self.azuriraj)
        brisanje = Button(self,text="Brisi")
        azuriranje.pack()
        brisanje.pack()
        en_fname = Entry(self)
        en_fname.place(x=10,y=10)
        en_lname = Entry(self)
        en_lname.place(x=10,y=50)
        self.lb_svi.bind("<<ListboxSelect>>",self.odabran)