import tkinter
from tkinter.ttk import *
import model

class AppFrame(tkinter.Frame):
    def unesi(self):
        model.insertuser(self.fn_var.get() , self.ln_var.get())
        #print(self.fn_var.get() + " " + self.ln_var.get())
    def dispose(self):
        pass
    def setcontrols(self,parentmodule): 
        self.fn_var = tkinter.StringVar(self)
        self.ln_var = tkinter.StringVar(self)
        taster1 = Button(self,text="Pocetak",command=lambda :parentmodule.switchpage('main'))
        taster1.pack() 
        Label(self,text="First Name:").pack()
        en_fn = Entry(self,textvariable=self.fn_var)
        en_fn.pack()
        Label(self,text="Last Name:").pack()
        en_ln = Entry(self,textvariable=self.ln_var)
        en_ln.pack()
        btn_unos = Button(self,text="Unesi",command=self.unesi)
        btn_unos.pack()
