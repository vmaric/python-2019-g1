import tkinter

win = tkinter.Tk()
win.geometry("600x400")

lbl = tkinter.Label(win,text="Dovla",bg="red")
lbl.pack()

brojklikova = 0

def klik(argumenti):
    global brojklikova
    brojklikova+=1
    if brojklikova > 20:
        win.unbind("<Button-1>",klik)
    novi_label = tkinter.Label(win,text="Dovla")
    novi_label.place(x=argumenti.x,y=argumenti.y)
    print(argumenti)

def klik1(argumenti):
    global brojklikova
    brojklikova+=1
    novi_label = tkinter.Label(win,text="Sasa")
    novi_label.place(x=argumenti.x,y=argumenti.y+20)
    print(argumenti)

def taster(argumenti):
    print(argumenti)
 
win.bind("<Button-1>",klik,add=True)
win.bind("<Button-1>",klik1,add=True)

lbl.focus()

lbl.bind("<KeyPress>",taster)

win.mainloop()