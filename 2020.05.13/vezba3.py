import torch
import torch.nn as nn

x = torch.tensor([[9,2,1,2],[2,8,2,3],[4,6,2,2],[7,3,3,2]],dtype=torch.float32)
y = torch.tensor([[0],[1],[1],[0]],dtype=torch.float32)

class Pucanje(nn.Module):
    def __init__(self):
        super().__init__()
        self.sloj1 = nn.Linear(4,2)
        self.sloj2 = nn.Linear(2,1)
    def forward(self,x):
        pred = self.sloj1(x)
        pred = self.sloj2(pred)
        return pred  

mreza = Pucanje()
f_greska = nn.MSELoss()
opt = torch.optim.SGD(mreza.parameters(),0.01)
sig = nn.Sigmoid()

for i in range(1000):
    pred = mreza(x)
    greska = f_greska(pred,y) 
    greska.backward()
    opt.step()
    opt.zero_grad() 

x_ = torch.tensor([1.0,5.0,2.0,3.2]) 
y_ = mreza(x_)
pogodak = sig(y_)
print(pogodak)
print(pogodak.item()>.5)