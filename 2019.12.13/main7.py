import random, time

PRVO_POLUVREME  = 1
PAUZA           = 2
DRUGO_POLUVREME = 3
 
while True:
    match_state = random.randint(1,3)
    if match_state == PRVO_POLUVREME:
        print("Prvo poluvreme je u toku")
    elif match_state == PAUZA:
        print("Pauza je u toku")
    else:
        print("Drugo poluvreme je u toku")
    time.sleep(2)