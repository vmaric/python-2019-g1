class Wtype:
    JEDI    = "Jedi"
    SITH    = "Sith"

class Weapon:
    name    = ""
    damage  = 0

class Warrior:
    def __init__(self,name,health,wpnname,wpndmg,tp):
        self.name             = name
        self.health           = health
        self.weapon           = Weapon()
        self.weapon.name      = wpnname
        self.weapon.damage    = wpndmg
        self.wtype            = tp
    def hit(self, drugi):
        drugi.health-=self.weapon.damage
    def show(self):
        print(self.name,self.weapon.name,self.weapon.damage,self.wtype,self.health)    