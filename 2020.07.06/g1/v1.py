from flask import Flask,request,session
import uuid,secrets
app = Flask(__name__)

app.config["SECRET_KEY"] = secrets.token_bytes(16)


@app.route("/",methods=["GET","POST"])
def naslovna():  
    if(request.method=="GET"):
        token = secrets.token_hex(16)
        session["token"] = token
        return f"<form method='post'><input type='hidden' value='{token}' name='token' /><input type='text' name='username' /><input type='text' name='password' /><input type='submit' value='Login' /></form>"
    else: 
        if not request.referrer:
            return "Molimo Vas pokusajte ponovo"
        if "http://127.0.0.1:5000/" != request.referrer:
            return "Postovani, Vi niste uputili zahtev sa odgovarajuce adrese"
        token = request.form.get("token")
        token_session = session["token"]
        if not token:
            return "Postovani...Vi nemate token?"
        if not token_session or token != token_session:
            return "Bezi propala hakercino" 
        un = request.form["username"]
        pwd = request.form["password"]
        
        return (un + " " + pwd)