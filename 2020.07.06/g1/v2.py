from flask import Flask,request,session
import mysql.connector as conn
import uuid,secrets
import hashlib
app = Flask(__name__)

db = conn.connect(username="root",host="localhost",passwd="",database="shop1") 

app.config["SECRET_KEY"] = secrets.token_bytes(16) 

@app.route("/product")
def product():
    pid = request.args["id"]
    cur = db.cursor()
    cur.execute(f"select * from phones where id = {pid}")
    products = cur.fetchall()
    output = ""
    for prod in products:
        output+=f"<div style='border:1px solid red;padding:4px;margin:4px;'>{prod[1]} {prod[2]}</div>"
    cur.close()
    return output

@app.route("/products")
def products():
    cur = db.cursor()
    cur.execute("select * from phones")
    products = cur.fetchall()
    output = ""
    for prod in products:
        output+=f"<a href='/product?id={prod[0]}'><div style='border:1px solid red;padding:4px;margin:4px;'>{prod[1]} {prod[2]}</div></a>"
    cur.close()
    return output

@app.route("/",methods=["GET","POST"])
def naslovna():  
    if(request.method=="GET"): 
        return f"<form method='post'><input type='text' name='username' /><input type='text' name='password' /><input type='submit' value='Login' /></form>"
    else:  
        un = request.form["username"]
        pwd = request.form["password"]  
        cur = db.cursor()  
        cur.execute(f"select * from users where username='{un}' limit 1") 
        user = cur.fetchone() 
        cur.close() 
        if user:
            hsh = hashlib.md5(pwd.encode()).hexdigest()
            if user[2] == hsh: 
                return f"Dobro dosao {user[1]}"
            else:
                return "Invalid user"
        return "Invalid user"