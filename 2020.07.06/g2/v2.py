from flask import Flask,request,session
import secrets
import mysql.connector as conn

def c():
    return conn.connect(username="root",passwd="",database="kucici",host="localhost")


app = Flask(__name__)

app.config['SECRET_KEY'] = secrets.token_bytes(8)

@app.route("/kuce",methods=["GET","POST"])
def details():
    kid = str(request.args["id"])
    if not kid.isnumeric():
        return "Hakujes, a?"
    db = c()
    cur = db.cursor()
    cur.execute(f"select * from kucici where id = {kid}")
    kucici = cur.fetchall()
    output = ""
    for kuce in kucici:
        k = f"<a href='?id={kuce[0]}'><div style='border:1px solid red;padding:4px;margin:4px;'>{kuce[1]} {kuce[2]}</div></a>"
        output+=k
    if request.method=="POST":
        komentar = request.form["komentar"]
        komentar = komentar.replace("<","&lt;")
        komentar = komentar.replace(">","&gt;")
        cur.execute("insert into komentari values (null,%s,%s)",(komentar,kid))
        db.commit()

    output+="<hr>"

    cur.execute("select * from komentari where pas = %s",(kid,))
    for kom in cur.fetchall(): 
        output+=kom[1]+"<hr>"
    
    output+="<form method='post'><textarea name='komentar'></textarea><br><input type='submit' value='Dodaj komentar' /></form>"

    db.close()
    return output

@app.route("/")
def home():
    db = c()
    cur = db.cursor()
    cur.execute("select id,naziv from kucici")
    kucici = cur.fetchall()
    output = ""
    for kuce in kucici:
        k = f"<a href='kuce?id={kuce[0]}'><div style='border:1px solid red;padding:4px;margin:4px;'>{kuce[1]}</div></a>"
        output+=k
    db.close()
    return output 

@app.route("/admin",methods=["GET","POST"])
def admin():
    if request.method == "GET":
        return f"<form method='post'>Username:<br><input type='text' name='username' /><br>Password:<br><input type='text' name='password' /><br><input type='submit' value='Login'/></form>"
    else:
        db = c()
        cur = db.cursor()
        uname = request.form["username"]
        pwd = request.form["password"]
        cur.execute(f"select * from users where username=%s and password = md5(%s) limit 1",(uname,pwd))
        user = cur.fetchone() 
        db.close()
        if user:
            return f"Dobro dosao {user[1]}"
        else:
            return "Invalid user"