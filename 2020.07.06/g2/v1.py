from flask import Flask,request,session
import secrets

app = Flask(__name__)

app.config['SECRET_KEY'] = secrets.token_bytes(8)


@app.route("/",methods=["GET","POST"])
def home():
    if request.method == "GET": 
        token = secrets.token_hex(8)
        session["token"] = token
        return f"<form method='post'><input type='hidden' name='token' value='{token}' />Username:<br><input type='text' name='username' /><br>Password:<br><input type='text' name='password' /><br><input type='submit' value='Login'/></form>"
    else:
        if not request.referrer:
            return "Invalid request 1"
        if request.referrer != "http://localhost:5000/":
            return "Invalid request 2"
        uname = request.form["username"]
        pwd = request.form["password"]
        token = request.form.get("token") 
        if not token:
            return "Invalid request 3"
        if not "token" in session:
            return "Invalid request 4"
        session_token = session["token"]
        if session_token != token:
            return "Invalid request 5"
        del session["token"]
        #autentifikacija
        return f"{uname} {pwd} {token} {session_token}"