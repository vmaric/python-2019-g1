#Gameplay Server

## Technology

Python with asyncio api. Work best on linux RHEL systems.

## Dependencies

- Redis database
- Mysql database

## Installation

Korak 1, korak 2, korak 3.....

[Vidi dokumentaciju baze](baza.md)

 
| parametar | vrednosti | opis |
----|----|----
| max_users | 0-100 | koliko maksimalno korisnika moze server da podnese |

## Konfiguracioni fajl
[Konfiguracioni fajl](fajlovi/install.txt)

## Slika opisa
![Screenshot](fajlovi/serliz.jpg)

[Screenshot](fajlovi/serliz.jpg)