# Scanner program

Program za skeniranje i prepoznavanje lica. Nalazi se na vratima, i omogucava otvaranje i zatvaranje. 
Izvrsava se na Rasberry Py 4.

## Startovanje
Mora da bude na linuxu centos 7. Startuje se komandom: nohup scanner.py

## Instalacija
* apdejtuj sistem
* instaliraj mysql
* instaliraj python 3.8

## Requirements

Polozen cambridge computing test

[Preuzmi dokument](static/camb.ppt)

![Najjaci kompjuter](static/zx81.jpg)

[Preuzmi sliku](static/zx81.jpg)