import string

title = "Player Name: "
username = "Vladimir"
full_title = title + username
print(full_title)

a = 4
b = 3.123456
c = a + b

title = "Result {2:b} + {1:.2f} is {0:.2f}"
title_formatted = title.format(c,b,a)
print(title_formatted)

a = 12
b = 23
c = a * b

title = f"Result {a} * {b} = {c}"
print(title) 

tmp = string.Template("Result $var1 * $var2 = $var3")

for i in range(10):
    vars = {
        "var1":a+i,
        "var2":b,
        "var3":(a+i)*b
    } 
    print(tmp.substitute(vars))