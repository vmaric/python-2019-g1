import http.server
import urllib.parse as parse
import http.cookies as cookies
import os 
import uuid
import datetime

#print(uuid.uuid1().hex) 

os.chdir("2020.05.04")

users = {
    "pera":"123",
    "sally":"234",
    "john":"345"
}

session_storage = {

}

class handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path.startswith("/logout"):
            ck = self.headers["Cookie"]
            if ck: 
                ckk = cookies.SimpleCookie()
                ckk.load(ck)
                session_id = ckk["PYSESSID"].value
                if session_id in session_storage:
                    del session_storage[session_id]  
            self.send_response(303) 
            self.flush_headers()
            ckk = cookies.SimpleCookie()
            ckk["PYSESSID"] = ""
            ckk["PYSESSID"]["max-age"] = 0
            self.wfile.write((ckk.output()+"\r\n").encode())
            self.send_header("location","/index.html")
            self.end_headers()
        elif self.path.startswith("/private.html"):
            ck = self.headers["Cookie"]
            if ck: 
                ckk = cookies.SimpleCookie()
                ckk.load(ck)
                session_id = ckk["PYSESSID"].value
                if session_id in session_storage:
                    sesija = session_storage[session_id]
                    ime = sesija["ime"]
                    ulogovan_od = sesija["logintime"]
                    self.send_response(200)
                    self.end_headers()
                    self.wfile.write(f"Welcome {ime}, you are here from {ulogovan_od}".encode())
                else:
                    self.wfile.write(f"Who are you?".encode())    
                self.wfile.flush()
            else:
                self.send_response(200)
                self.end_headers()
                self.wfile.write(b"Nedozvoljen pristup")
                self.wfile.flush()
        else:
            super().do_GET()
    def do_POST(self):
        length = int(self.headers["content-length"])
        content = self.rfile.read(length)
        content = content.decode('utf-8')
        content = dict(parse.parse_qsl(content)) 
        username = content["username"]
        passwd = content["passwd"]
        user_valid = False
        if username in users and users[username] == passwd:
            user_valid = True
        #sql = f"select * from users where username = '{username}' and passwd = '{passwd}'"
        #print(sql)
        self.send_response(200) 
        self.flush_headers()
        if user_valid:
            ck = cookies.SimpleCookie()
            session_id = uuid.uuid1().hex
            session_storage[session_id] = {
                "ime":username,
                "logintime":datetime.datetime.now()
            }
            ck["PYSESSID"] = session_id
            self.wfile.write(ck.output().encode()) 
            self.wfile.write(b"\r\n")
            #self.send_header("Set-Cookie","poruka=Hello; max-age=20")
        self.end_headers()
        
        if user_valid:
            self.wfile.write(b"Welcome my son")
        else:
            self.wfile.write(b"No bread from hacking")
        self.wfile.flush()

http.server.HTTPServer(("localhost",80),handler).serve_forever()




