def f(i):
    return i*f(i-1) if i>1 else 1

print(f(5))