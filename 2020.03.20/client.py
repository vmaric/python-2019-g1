import socket 
while True:
    print("1. Svi kursevi, 2. Po strani, 3. Po id-u, 0. izlaz")
    cmd = int(input("Unesi komandu:"))
    client = socket.socket() 
    client.connect(("gubitnik.com",8005)) 
    if cmd==1:
        client.send(b"1|0")
    elif cmd==2:
        strana = int(input("Unesi stranu:"))
        client.send(f"2|{strana}".encode("utf-8"))
    elif cmd==3:
        id = int(input("Unesi id:"))
        client.send(f"3|{id}".encode("utf-8")) 
    elif cmd==0:
        print("Bye bye")
        exit(0)
    print(client.recv(1024).decode("utf-8"))
    client.close()