import mysql.connector as conn 
import json,pickle,base64
import asyncio

def getcourses(page=1,id=-1):
    page = (page-1) * 5
    db = conn.connect(
        host="localhost",
        database="g2_courses",
        username="root",
        passwd=""
    )
    cur = db.cursor()   
    res = []   
    if id>0:
        cur.execute(f"select courses.id,courses.title,courses.price,categories.name from courses join categories on courses.cat = categories.id where courses.id = {id}")
    else:
        cur.execute(f"select courses.id,courses.title,courses.price,categories.name from courses join categories on courses.cat = categories.id limit {page},5")
    for course in cur.fetchall():
        res.append((course[0],course[1],float(course[2]),course[3]))
    db.close()
    return res



async def cbf(r,w):
    print("Connection accepted...")
    pitanje     = await r.read(16)
    pitanje     = pitanje.decode("utf-8").split("|")
    komanda     = int(pitanje[0])
    parametar   = int(pitanje[1])  
    kursevi = None 
    if komanda == 1:
        kursevi = getcourses()
    elif komanda == 2:
        kursevi = getcourses(page=parametar)
    elif komanda == 3:
        kursevi = getcourses(page=parametar,id=parametar) 
    out = "################# Dostupni kursevi ########################\n"
    for course in kursevi:
        out+=f"Id kursa: {course[0]}, naziv kursa: {course[1]} cena: {course[2]} kategorija: {course[3]}\n"
    out += "################# Dostupni kursevi ########################"
    w.write(out.encode("utf-8"))
    w.close()
    print("Connection has been closed")

loop = asyncio.get_event_loop() 
server = asyncio.start_server(cbf,host="localhost",port=8005) 
print("Server is running: ")
loop.run_until_complete(server) 
loop.run_forever()